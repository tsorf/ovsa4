package ru.frosteye.ovsa.presentation.view.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import androidx.appcompat.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.Scroller
import android.widget.TextView



/**
 * A TextView that scrolls it contents across the screen, in a similar fashion as movie credits roll
 * across the theater screen.
 *
 * @author Matthias Kaeppler
 */
class ScrollingTextView : AppCompatTextView, Runnable {

    var customScroller: Scroller? = null
    var speed = DEFAULT_SPEED
    var isContinuousScrolling = true

    constructor(context: Context) : super(context) {
        setup(context)
    }

    constructor(context: Context, attributes: AttributeSet) : super(context, attributes) {
        setup(context)
    }

    private fun setup(context: Context) {
        customScroller = Scroller(context, LinearInterpolator())
        setScroller(customScroller)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (customScroller!!.isFinished) {
            scroll()
        }
    }

    private fun scroll() {
        val viewHeight = height
        val visibleHeight = viewHeight - paddingBottom - paddingTop
        val lineHeight = lineHeight

        val offset = -1 * visibleHeight
        val distance = visibleHeight + lineCount * lineHeight
        val duration = (distance * speed).toInt()

        customScroller!!.startScroll(0, offset, 0, distance, duration)

        if (isContinuousScrolling) {
            post(this)
        }
    }

    override fun run() {
        if (customScroller!!.isFinished) {
            scroll()
        } else {
            post(this)
        }
    }

    companion object {

        private val DEFAULT_SPEED = 15.0f
    }
}

open class VerticalTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {
    private val topDown: Boolean = false

    init {
        /*val gravity = gravity
        topDown = if (Gravity.isVertical(gravity) && gravity and Gravity.VERTICAL_GRAVITY_MASK == Gravity.BOTTOM) {
            setGravity(gravity and Gravity.HORIZONTAL_GRAVITY_MASK or Gravity.TOP)
            false
        } else
            true*/
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec)
        setMeasuredDimension(measuredHeight, measuredWidth)
    }

    override fun onDraw(canvas: Canvas) {
        val textPaint = paint
        textPaint.color = currentTextColor
        textPaint.drawableState = drawableState

        canvas.save()

        if (topDown) {
            canvas.translate(width.toFloat(), 0f)
            canvas.rotate(90f)
        } else {
            canvas.translate(0f, height.toFloat())
            canvas.rotate(-90f)
        }


        canvas.translate(compoundPaddingLeft.toFloat(), extendedPaddingTop.toFloat())

        layout.draw(canvas)
        canvas.restore()
    }
}





class BlinkingTextView : AppCompatTextView {

    private var anim: Animation? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onFinishInflate() {
        super.onFinishInflate()
        start()
    }

    var duration: Long = 1000

    fun start() {
        stop()
        anim = AlphaAnimation(0.4f, 1.0f)
        anim!!.duration = duration
        anim!!.startOffset = 20
        anim!!.repeatMode = Animation.REVERSE
        anim!!.repeatCount = Animation.INFINITE
        startAnimation(anim)
    }

    fun stop() {
        if (anim != null) {
            anim!!.cancel()
            anim = null
        }
        alpha = 1.0f
    }

    override fun setVisibility(visibility: Int) {
        if (visibility == View.GONE) {
            clearAnimation()
        } else {
            start()
        }
        super.setVisibility(visibility)
    }
}



class UnderlinedTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    fun showLine(shown: Boolean) {
        paintFlags = if (shown) {
            paintFlags or Paint.UNDERLINE_TEXT_FLAG
        } else {
            paintFlags and Paint.UNDERLINE_TEXT_FLAG.inv()
        }
        invalidate()
    }
}