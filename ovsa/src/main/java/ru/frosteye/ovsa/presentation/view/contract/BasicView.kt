package ru.frosteye.ovsa.presentation.view.contract




interface BasicView {

    fun enableControls(enabled: Boolean, code: Int = 0)
    fun showMessage(message: CharSequence?, code: Int)


    companion object {

        val CONTROLS_CODE_ERROR = -1
        val CONTROLS_CODE_SUCCESS = 1
        val CONTROLS_CODE_PROGRESS = 0
    }

}
