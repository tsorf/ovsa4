package ru.frosteye.ovsa.presentation.effect

import android.util.Log
import android.view.MotionEvent
import android.view.View

class VerticalSwipeTouchListener(
    private val onDismiss: () -> Unit,
    private val onCancel: () -> Unit
) : View.OnTouchListener {

    private var initialViewHeight: Int = 0
    private var dismissed = false
    private var dY = 0f
    private var initialDY = 0f

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        Log.i("!@#", event.toString())

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                dismissed = false
                initialViewHeight = v.measuredHeight
                dY = v.y - event.y
            }

            MotionEvent.ACTION_MOVE -> {
                if (dismissed || dY < 0)
                    return false
                Log.i("!@#", event.rawY.toString())
                if (initialDY == 0f) {
                    initialDY = event.rawY
                }
                dY = event.rawY - initialDY
                if (dY > initialViewHeight / 3) {
                    onDismiss()
                    dismissed = true
                } else {
                    v.translationY = dY
                }
            }

            MotionEvent.ACTION_UP -> {
                v.performClick()
                initialViewHeight = 0
                dY = 0f
                if (!dismissed && initialDY != 0f) {
                    onCancel()
                    dismissed = true
                }
                if (initialDY != 0f) {
                    initialDY = 0f
                    return true
                }
            }

        }
        return false
    }

}