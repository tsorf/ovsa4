package ru.frosteye.ovsa.presentation.view.dialog

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import androidx.annotation.LayoutRes
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView
import ru.frosteye.ovsa.tool.common.applyLayoutDimensions
import ru.frosteye.ovsa.tool.inflateAs

open class DialogCompanion<T, V>(
    @LayoutRes val layoutResource: Int
) where V : InteractiveModelView<T>, V : View {

    fun dialog(context: Context, model: T, listener: InteractiveModelView.Listener): Dialog {
        val view = layoutResource.inflateAs<V>(context)
        return Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(view)
            view.model = model
            view.listener = listener
            show()
            applyLayoutDimensions(-1, -2)
        }
    }


}