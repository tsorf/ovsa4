package ru.frosteye.ovsa.presentation.view.widget.impl

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.frosteye.ovsa.tool.dpToPx

class TrailingMarginDecoration(
    private val marginInPx: Float,
    private val direction: Int = RecyclerView.VERTICAL
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        parent.adapter?.let {

            if (parent.getChildAdapterPosition(view) == it.itemCount - 1) {
                if(direction == RecyclerView.VERTICAL) {
                    outRect.bottom = marginInPx.toInt()
                } else {
                    outRect.right = marginInPx.toInt()
                }
            }
        }
    }
}

fun RecyclerView.trailing(marginDp: Float) {
    var orientation = RecyclerView.VERTICAL
    (layoutManager as? LinearLayoutManager)?.let {
        orientation = it.orientation
    }
    addItemDecoration(TrailingMarginDecoration(dpToPx(marginDp), orientation))
}