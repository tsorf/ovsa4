package ru.frosteye.ovsa.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import ru.frosteye.ovsa.presentation.view.contract.ModelView
import ru.frosteye.ovsa.presentation.view.contract.Positioned
import ru.frosteye.ovsa.presentation.view.contract.ViewHolderChild



abstract class AdapterItem<T, V>(val model: T) :
    AbstractFlexibleItem<ModelViewHolder<V>>() where V : View, V : ModelView<T> {

    abstract val layoutResource: Int

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as AdapterItem<*, *>?

        return if (model != null) model == that!!.model else that!!.model == null

    }

    open override fun getLayoutRes(): Int {
        return layoutResource
    }

    override fun hashCode(): Int {
        return if (model != null) model!!.hashCode() else 0
    }

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ): ModelViewHolder<V> {
        val holder = ModelViewHolder<V>(view, adapter)
        if (adapter is FlexibleModelAdapter<*>) {
            holder.setListener(adapter.listener)
        }
        onViewCreated(holder.view)
        return holder
    }
    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<*>>, holder: ModelViewHolder<V>,
        position: Int, payloads: List<Any>
    ) {
        if (holder.view is Positioned) {
            holder.view.position = holder.adapterPosition
        }
        if (holder.view is ViewHolderChild) {
            holder.view.viewHolder = holder
        }
        beforeBindModel(holder.view)
        holder.view.model = model
        afterBindModel(holder.view)
    }

    open fun onViewCreated(view: V) {

    }

    open fun beforeBindModel(view: V) {

    }

    open fun afterBindModel(view: V) {

    }
}

abstract class StaticAdapterItem : AbstractFlexibleItem<StaticFlexibleViewHolder>() {

    abstract val layoutResource: Int



    open override fun getLayoutRes(): Int {
        return layoutResource
    }

    override fun createViewHolder(
            view: View,
            adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ): StaticFlexibleViewHolder {
        return StaticFlexibleViewHolder(view, adapter)
    }



    override fun bindViewHolder(
            adapter: FlexibleAdapter<IFlexible<*>>, holder: StaticFlexibleViewHolder,
            position: Int, payloads: List<Any>
    ) {
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StaticAdapterItem

        if (layoutResource != other.layoutResource) return false

        return true
    }

    override fun hashCode(): Int {
        return layoutResource
    }
}
