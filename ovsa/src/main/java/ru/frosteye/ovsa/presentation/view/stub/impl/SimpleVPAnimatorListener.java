package ru.frosteye.ovsa.presentation.view.stub.impl;

import android.view.View;

import androidx.core.view.ViewPropertyAnimatorListener;

public class SimpleVPAnimatorListener implements ViewPropertyAnimatorListener {
    @Override
    public void onAnimationStart(View view) {

    }

    @Override
    public void onAnimationEnd(View view) {

    }

    @Override
    public void onAnimationCancel(View view) {

    }
}
