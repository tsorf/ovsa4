package ru.frosteye.ovsa.presentation.view.widget.coordinator

import android.content.Context
import android.util.AttributeSet
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout

class StatedAppBarLayout : AppBarLayout, AppBarLayout.OnOffsetChangedListener {

    private var state: State? = null

    var onStateChangeListener: OnStateChangeListener? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (layoutParams !is CoordinatorLayout.LayoutParams || parent !is CoordinatorLayout) {
            throw IllegalStateException(
                    "FixedAppBarLayout must be a direct child of CoordinatorLayout.")
        }
        addOnOffsetChangedListener(this)
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
        if (verticalOffset == 0) {
            if (onStateChangeListener != null && state != State.EXPANDED) {
                onStateChangeListener?.onStateChange(State.EXPANDED)
            }
            state = State.EXPANDED
        } else if (Math.abs(verticalOffset) >= appBarLayout.totalScrollRange) {
            if (onStateChangeListener != null && state != State.COLLAPSED) {
                onStateChangeListener?.onStateChange(State.COLLAPSED)
            }
            state = State.COLLAPSED
        } else {
            if (onStateChangeListener != null && state != State.IDLE) {
                onStateChangeListener?.onStateChange(State.IDLE)
            }
            state = State.IDLE
        }
    }

    interface OnStateChangeListener {
        fun onStateChange(toolbarChange: State)
    }

    enum class State {
        COLLAPSED,
        EXPANDED,
        IDLE
    }
} 