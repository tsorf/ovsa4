package ru.frosteye.ovsa.presentation.view.widget

import androidx.core.view.ViewCompat
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener


fun View.elevate(elevation: Float) {
    ViewCompat.setElevation(this, elevation)
}

fun View.waitForSizes(listener: (w: Int, h: Int) -> Unit) {
    val viewTreeObserver = viewTreeObserver
    if (viewTreeObserver.isAlive) {
        viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                this@waitForSizes.viewTreeObserver.removeOnGlobalLayoutListener(this)
                listener(width, height)
            }
        })
    }
}