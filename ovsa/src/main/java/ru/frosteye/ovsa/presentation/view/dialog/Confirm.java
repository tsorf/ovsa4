package ru.frosteye.ovsa.presentation.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

import ru.frosteye.ovsa.R;


public class Confirm {
    private AlertDialog.Builder builder;
    private int yesTitle = R.string.ovsa_string_ok;
    private int noTitle = R.string.ovsa_string_cancel;
    private int neutralTitle = 0;
    private int title = R.string.ovsa_string_confirm_title;
    private int message = R.string.ovsa_string_confirm_message;
    private CharSequence messageText;
    private CharSequence titleText;

    private DialogInterface.OnClickListener yesOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.cancel();
        }
    };
    private DialogInterface.OnClickListener noOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.cancel();
        }
    };
    private DialogInterface.OnClickListener neutralOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.cancel();
        }
    };

    private Confirm(Context context) {
        this.builder = new AlertDialog.Builder(context);
    }



    public Confirm yesTitle(int yesTitle) {
        this.yesTitle = yesTitle;
        return this;
    }

    public Confirm noTitle(int noTitle) {
        this.noTitle = noTitle;
        return this;
    }

    public Confirm yes(int yesTitle, DialogInterface.OnClickListener onClickListener) {
        this.yesOnClickListener = onClickListener;
        this.yesTitle = yesTitle;
        return this;
    }

    public Confirm no(int noTitle, DialogInterface.OnClickListener onClickListener) {
        this.noOnClickListener = onClickListener;
        this.noTitle = noTitle;
        return this;
    }

    public Confirm neutral(int neutralTitle, DialogInterface.OnClickListener onClickListener) {
        this.neutralOnClickListener = onClickListener;
        this.neutralTitle = neutralTitle;
        return this;
    }

    public Confirm yes(DialogInterface.OnClickListener onClickListener) {
        this.yesOnClickListener = onClickListener;
        return this;
    }

    public Confirm no(DialogInterface.OnClickListener onClickListener) {
        this.noOnClickListener = onClickListener;
        return this;
    }

    public Confirm title(int title) {
        this.title = title;
        return this;
    }

    public Confirm title(CharSequence title) {
        this.titleText = title;
        return this;
    }

    public Confirm message(int message) {
        this.message = message;
        return this;
    }

    public Confirm message(CharSequence message) {
        this.messageText = message;
        return this;
    }

    public AlertDialog show() {
        builder.setTitle(title);
        builder.setMessage(message);
        if(titleText != null) builder.setTitle(titleText);
        if(messageText != null) builder.setMessage(messageText);
        builder
                .setPositiveButton(yesTitle, yesOnClickListener)
                .setNegativeButton(noTitle, noOnClickListener);
        if(neutralTitle != 0)
            builder.setNeutralButton(neutralTitle, neutralOnClickListener);
        return builder.show();
    }

    public static Confirm create(Context context) {
        return new Confirm(context);
    }
}
