package ru.frosteye.ovsa.presentation.presenter



import android.content.Intent

import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity

/**
 * [Presenter] with some lifecycle (should be bound to [PresenterActivity]'s lifecycle)
 *
 * @param <T> [Presenter]'s view type
</T> */
interface LivePresenter<T> : Presenter<T> {
    fun onResume()

    fun onPause()

    fun onStop()

    fun onDestroy()

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun onSetIntent(intent: Intent)
}
