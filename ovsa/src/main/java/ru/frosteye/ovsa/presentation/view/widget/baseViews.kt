package ru.frosteye.ovsa.presentation.view.widget

import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import ru.frosteye.ovsa.tool.common.UITools
import ru.frosteye.ovsa.presentation.adapter.ModelViewHolder
import ru.frosteye.ovsa.presentation.view.contract.ViewHolderChild

@Deprecated("just don't")
fun View.getStyleable(context: Context): IntArray? {
    return try {
        UITools.getResourceDeclareStyleableIntArray(context, javaClass.simpleName)
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

abstract class BaseConstraintLayout : ConstraintLayout, ViewHolderChild {


    open val layoutToInflate: Int = 0

    override var position: Int = -1
        get() {
            viewHolder?.let {
                return it.adapterPosition
            } ?: run {
                return field
            }
        }


    override var viewHolder: ModelViewHolder<*>? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}



abstract class BaseFrameLayout : FrameLayout, ViewHolderChild {

    protected open val layoutToInflate: Int
        @LayoutRes
        get() = 0

    override var position: Int = -1
        get() {
            viewHolder?.let {
                return it.adapterPosition
            } ?: run {
                return field
            }
        }

    protected open fun getStyleableResource(context: Context): IntArray? {
        return getStyleable(context)
    }

    override var viewHolder: ModelViewHolder<*>? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val styleableResource = getStyleableResource(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleableResource(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context, attrs: AttributeSet, defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        val styleableResource = getStyleableResource(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}



abstract class BaseImageView : AppCompatImageView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

abstract class BaseView : View {
    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

abstract class BaseLinearLayout : LinearLayout, ViewHolderChild {

    open val layoutToInflate: Int = 0

    open val styleableResource: IntArray? = null

    override var position: Int = -1
        get() {
            viewHolder?.let {
                return it.adapterPosition
            } ?: run {
                return field
            }
        }

    override var viewHolder: ModelViewHolder<*>? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context) ?: this.styleableResource
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context) ?: this.styleableResource
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context, attrs: AttributeSet?, defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        val styleableResource = getStyleable(context) ?: this.styleableResource
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}



abstract class BaseRelativeLayout : RelativeLayout {

    protected open val layoutToInflate: Int
        @LayoutRes
        get() = 0

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context, attrs: AttributeSet?, defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()


}



abstract class BaseTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

abstract class BaseTextInputView : TextInputEditText {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

abstract class BaseTextInputLayoutView : TextInputLayout {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}