package ru.frosteye.ovsa.presentation.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import com.tbruyelle.rxpermissions2.RxPermissions

import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import ru.frosteye.ovsa.presentation.presenter.Presenter
import ru.frosteye.ovsa.presentation.view.contract.BasicView
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView
import java.lang.Exception
import java.lang.RuntimeException


abstract class PresenterActivity : OvsaActivity(), BasicView, InteractiveModelView.Listener {

    lateinit var permissions: RxPermissions
        private set

    private var savedInstanceState: Bundle? = null
    var isEnabled: Boolean = true
        private set

    /**
     * Activity's presenter getter.
     * @return The [LivePresenter] instance, associated with this activity.
     */
    protected abstract val presenter: LivePresenter<*>?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissions = RxPermissions(this)
        this.savedInstanceState = savedInstanceState
    }

    protected open fun inject(component: Any) {
        try {
            component.javaClass
                    .getMethod("inject", javaClass)
                    .invoke(component, this)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    override fun onModelAction(code: Int, payload: Any?) {

    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        prepareView()
        initView()
        initView(savedInstanceState)
        presenter?.onSetIntent(intent)
        attachPresenter()
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)
        prepareView()
        initView()
        initView(savedInstanceState)
        presenter?.onSetIntent(intent)
        attachPresenter()
    }

    @CallSuper
    override fun enableControls(enabled: Boolean, code: Int) {
        this.isEnabled = enabled
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter?.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Override to perform any view preparations - ButterKnife.bind() etc.
     */
    @Deprecated("not used in most cases")
    protected open fun prepareView() {}

    /**
     * This activity's view initialization point.
     */
    @Deprecated("use initView(Bundle savedInstanceState)")
    protected fun initView() {

    }

    protected open fun initView(savedInstanceState: Bundle?) {

    }

    /**
     * Should contain activity's presenter initialization logic. Be sure to call [Presenter.onAttach] here.
     */
    protected abstract fun attachPresenter()
}
