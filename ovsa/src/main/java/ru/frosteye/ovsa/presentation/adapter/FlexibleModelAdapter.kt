package ru.frosteye.ovsa.presentation.adapter

import java.util.ArrayList

import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView



open class FlexibleModelAdapter<T : IFlexible<*>> : FlexibleAdapter<T> {

    var listener: InteractiveModelView.Listener? = null
        private set

    constructor(items: List<T>?) : super(items) {}

    constructor(items: List<T>?, listeners: Any?) : super(items, listeners) {}

    constructor(items: List<T>?, listeners: Any?,
                stableIds: Boolean) : super(items, listeners, stableIds) {
    }

    constructor(items: List<T>?, listener: InteractiveModelView.Listener) : super(items) {
        this.listener = listener
    }

    constructor(items: List<T>?, listeners: Any?,
                listener: InteractiveModelView.Listener) : super(items, listeners) {
        this.listener = listener
    }

    constructor(listener: InteractiveModelView.Listener) : super(ArrayList<T>()) {
        this.listener = listener
    }

    constructor(items: List<T>?, listeners: Any?,
                stableIds: Boolean, listener: InteractiveModelView.Listener) : super(items, listeners, stableIds) {
        this.listener = listener
    }
}

fun <T : IFlexible<*>> FlexibleAdapter<T>.getAllItems(): List<T> {
    val result = mutableListOf<T>()
    var i = 0
    while (i < itemCount) {
        getItem(i)?.let {
            result.add(it)
        }
        i++
    }
    return result
}
