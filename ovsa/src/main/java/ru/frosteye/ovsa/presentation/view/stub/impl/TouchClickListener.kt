package ru.frosteye.ovsa.presentation.view.stub.impl

import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

class TouchClickListener(val threshold: Int = 200, private val listener: (x: Int, y: Int) -> Unit) :
    View.OnTouchListener {
    var startX: Float = 0f
    var startY: Float = 0f

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                return true
            }
            MotionEvent.ACTION_UP -> {
                val endX = event.x
                val endY = event.y
                if (isAClick(startX, endX, startY, endY)) {
                    v.performClick()
                    listener(startX.toInt(), startY.toInt())
                    return true
                }
            }
        }

        return false
    }

    private fun isAClick(startX: Float, endX: Float, startY: Float, endY: Float): Boolean {
        val differenceX = abs(startX - endX)
        val differenceY = abs(startY - endY)
        return !(differenceX > threshold || differenceY > threshold)
    }
}