package ru.frosteye.ovsa.presentation.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Single
import io.reactivex.disposables.Disposable

import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView
import java.lang.Exception
import java.lang.RuntimeException



abstract class PresenterFragment : OvsaFragment(), InteractiveModelView.Listener {


    lateinit var permissions: RxPermissions
        private set

    protected abstract val presenter: LivePresenter<*>?
    protected var call: Disposable? = null
    var isEnabled: Boolean = true
        private set

    override fun onAttach(context: Context) {
        super.onAttach(context)
        permissions = RxPermissions(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        prepareView(view)
        initView(view, savedInstanceState)
        initView(view)
        presenter?.onSetIntent(Intent().apply {
            putExtra("fragment", this@PresenterFragment.javaClass.name)
            arguments?.let {
                putExtras(it)
            }
        })
        attachPresenter()
    }

    protected fun inject(component: Any) {
        try {
            component.javaClass
                .getMethod("inject", javaClass)
                .invoke(component, this)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    @CallSuper
    open fun enableControls(enabled: Boolean, code: Int) {
        this.isEnabled = enabled
    }

    protected open fun prepareView(view: View) {}

    /**
     * @param view
     */
    @Deprecated("use initView(View view, Bundle savedInstanceState)")
    protected open fun initView(view: View) {

    }

    protected open fun initView(view: View, savedInstanceState: Bundle?) {

    }

    override fun onModelAction(code: Int, payload: Any?) {

    }

    protected abstract fun attachPresenter()

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }

    protected fun <T> async(
        block: () -> T, result: (T) -> Unit,
        error: ((Throwable) -> Unit)? = null
    ) {
        call = Single.create<T> {
            try {
                it.onSuccess(block())
            } catch (e: Exception) {
                it.onError(e)
                e.printStackTrace()
            }
        }.subscribe({
            result(it)
        }, {
            error?.invoke(it)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        call?.dispose()
        presenter?.onDestroy()
    }


}
