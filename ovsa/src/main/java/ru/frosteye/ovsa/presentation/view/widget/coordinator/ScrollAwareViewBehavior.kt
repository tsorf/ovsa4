package ru.frosteye.ovsa.presentation.view.widget.coordinator

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton


class ScrollAwareViewBehavior(context: Context, attrs: AttributeSet) : CoordinatorLayout.Behavior<View>() {

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: View,
                                     directTargetChild: View, target: View, nestedScrollAxes: Int): Boolean {
        return true
    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout, child: View, target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int, type: Int, consumed: IntArray) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type, consumed)
        if (dyConsumed > 0 && child.visibility == View.GONE) {
            show(child)
        } else if (dyConsumed < 0 && child.visibility == View.VISIBLE) {
            hide(child)
        }
    }

    private fun show(view: View) {
        val translate = view.measuredHeight.toFloat()
        view.apply {
            translationY = translate
            visibility = View.VISIBLE
        }
        ViewCompat.animate(view)
            .setDuration(300)
            .translationY(0f)
            .setInterpolator(AccelerateInterpolator())
            .withEndAction {
                view.visibility = View.VISIBLE
            }
    }

    private fun hide(view: View) {
        val translate = view.measuredHeight.toFloat()
        view.apply {
            visibility = View.VISIBLE
        }
        ViewCompat.animate(view)
            .setDuration(300)
            .translationY(-translate)
            .setInterpolator(AccelerateInterpolator())
            .withEndAction {
                view.visibility = View.GONE
            }

    }
}
