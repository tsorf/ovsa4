package ru.frosteye.ovsa.presentation.view.widget.impl

import android.content.Context
import android.util.AttributeSet
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout


open class RefreshableSwipeRefreshLayout : SwipeRefreshLayout {
    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun setRefreshing(refreshing: Boolean) {
        if (!refreshing) {
            post { super@RefreshableSwipeRefreshLayout.setRefreshing(false) }
        } else {
            post { super@RefreshableSwipeRefreshLayout.setRefreshing(true) }
        }
    }
}