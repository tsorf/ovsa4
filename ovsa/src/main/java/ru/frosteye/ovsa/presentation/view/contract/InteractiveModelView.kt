package ru.frosteye.ovsa.presentation.view.contract

import ru.frosteye.ovsa.presentation.adapter.ModelViewHolder



interface InteractiveModelView<T> : ModelView<T> {
    var listener: Listener?

    interface Listener {
        fun onModelAction(code: Int, payload: Any? = null)
    }
}

interface Positioned {

    var position: Int

    data class Holder(
        val position: Int,
        val payload: Any? = null
    ) {

        fun <T>payload(): T? {
            return payload as T?
        }
    }
}

interface ViewHolderChild : Positioned {

    var viewHolder: ModelViewHolder<*>?
}


