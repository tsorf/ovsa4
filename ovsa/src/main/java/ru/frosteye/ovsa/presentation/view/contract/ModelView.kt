package ru.frosteye.ovsa.presentation.view.contract

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


interface ModelView<T> {
    var model: T
}

@Deprecated("not used?")
interface ConsumerView<T> : BasicView {
    var model: T?
}


public fun <T : Any> setter(
    set: (T) -> Unit
): LateInit<T> = LateInitImpl({ value }, {
    value = it
    set(value)
})

interface LateInit<T : Any> : ReadWriteProperty<Any?, T> {

    interface FieldHolder<T> {
        var value: T
    }
}


class LateInitImpl<T : Any>(
    private val getter: FieldHolderImpl<T>.() -> T = { value },
    private val setter: FieldHolderImpl<T>.(T) -> Unit = { value = it }
) : LateInit<T> {
    private val fieldHolder = FieldHolderImpl<T>()

    override fun getValue(thisRef: Any?, property: KProperty<*>) = fieldHolder.getter()
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) =
        fieldHolder.setter(value)

    class FieldHolderImpl<T : Any> : LateInit.FieldHolder<T> {
        override lateinit var value: T
    }
}
