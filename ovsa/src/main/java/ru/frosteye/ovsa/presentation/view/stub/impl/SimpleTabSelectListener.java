package ru.frosteye.ovsa.presentation.view.stub.impl;


import com.google.android.material.tabs.TabLayout;


public class SimpleTabSelectListener implements TabLayout.OnTabSelectedListener {
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
