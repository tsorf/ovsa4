package ru.frosteye.ovsa.presentation.view.widget.coordinator

import android.content.Context
import androidx.appcompat.widget.Toolbar
import android.text.TextUtils
import android.util.AttributeSet
import android.widget.TextView



class MarqueeToolbar : Toolbar {

    internal var title: TextView? = null
    internal var subTitle: TextView? = null

    internal var reflected = false

    internal var reflectedSub = false

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun setTitle(title: CharSequence) {

        reflected = reflectTitle()

        super.setTitle(title)
        selectTitle()
    }

    override fun setTitle(resId: Int) {
        if (!reflected) {
            reflected = reflectTitle()
        }
        super.setTitle(resId)
        selectTitle()
    }

    private fun reflectTitle(): Boolean {
        try {
            val field = Toolbar::class.java.getDeclaredField("mTitleTextView")
            field.isAccessible = true
            title = field.get(this) as TextView
            title!!.ellipsize = TextUtils.TruncateAt.MARQUEE
            title!!.marqueeRepeatLimit = -1
            return true
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
            return false
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
            return false
        } catch (e: NullPointerException) {
            e.printStackTrace()
            return false
        }

    }

    fun selectTitle() {
        if (title != null)
            title!!.isSelected = true
    }

    // ------------ for Subtitle ----------

    override fun setSubtitle(subTitle: CharSequence) {
        if (!reflectedSub) {
            reflectedSub = reflectSubTitle()
        }
        super.setSubtitle(subTitle)
        selectSubtitle()
    }


    override fun setSubtitle(resId: Int) {
        if (!reflected) {
            reflectedSub = reflectSubTitle()
        }
        super.setSubtitle(resId)
        selectSubtitle()
    }

    private fun reflectSubTitle(): Boolean {
        try {
            val field = Toolbar::class.java.getDeclaredField("mSubtitleTextView")
            field.isAccessible = true
            subTitle = field.get(this) as TextView
            subTitle!!.ellipsize = TextUtils.TruncateAt.MARQUEE
            subTitle!!.marqueeRepeatLimit = -1
            return true
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
            return false
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
            return false
        } catch (e: NullPointerException) {
            e.printStackTrace()
            return false
        }

    }

    fun selectSubtitle() {
        if (subTitle != null)
            subTitle!!.isSelected = true
    }

}

