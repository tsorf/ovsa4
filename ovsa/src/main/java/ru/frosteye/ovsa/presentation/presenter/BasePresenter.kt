package ru.frosteye.ovsa.presentation.presenter

import android.content.Intent
import android.os.Parcelable
import androidx.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

import ru.frosteye.ovsa.presentation.view.contract.BasicView



abstract class BasePresenter<V : BasicView> : LivePresenter<V> {

    protected lateinit var view: V
    protected lateinit var intent: Intent
        private set

    @Deprecated("use multiDisposable")
    protected var call: Disposable? = null
        set(value) {
            val current = call
            if (current != null && !current.isDisposed) {
                current.dispose()
            }
            field = value
        }

    private val multiDisposable: CompositeDisposable = CompositeDisposable()


    operator fun Disposable.unaryPlus() {
        multiDisposable.add(this)
    }

    operator fun Disposable.unaryMinus() {
        multiDisposable.remove(this)
    }

    @CallSuper
    override fun onAttach(v: V, vararg params: Any) {
        this.view = v
    }

    /*@CallSuper
    @Deprecated("use onAttach with params")
    open fun onAttach(v: V) {
    }*/

    override fun onResume() {

    }

    @CallSuper
    override fun onSetIntent(intent: Intent) {
        this.intent = intent
    }

    protected fun <T : Parcelable> getIntentPayload(name: String): T? {
        return intent.getParcelableExtra(name)
    }

    protected inline fun <reified T : Parcelable> getIntentPayload(): T? {
        return intent.getParcelableExtra(T::class.java.name)
    }

    override fun onPause() {

    }

    @CallSuper
    override fun onDestroy() {
        call?.dispose()
        multiDisposable.dispose()
    }

    override fun onStop() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    protected fun showMessage(message: CharSequence?) {
        view.showMessage(message, 0)
    }

    protected fun showError(message: CharSequence?) {
        view.showMessage(message, -1)
    }

    protected fun showSuccess(message: CharSequence?) {
        view.showMessage(message, 1)
    }

    protected fun enableControls(enabled: Boolean, code: Int = 0) {
        view.enableControls(enabled, code)
    }
}
