package ru.frosteye.ovsa.presentation.adapter

import androidx.core.view.ViewPropertyAnimatorListener
import android.view.View

import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.AnimatedViewHolder
import eu.davidea.viewholders.FlexibleViewHolder
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView



class ModelViewHolder<V : View>(view: View, adapter: FlexibleAdapter<*>) :
        FlexibleViewHolder(view, adapter), AnimatedViewHolder {

    override fun preAnimateAddImpl(): Boolean {
        return if (view is AnimatedViewHolder) {
            view.preAnimateAddImpl()
        } else false
    }

    override fun preAnimateRemoveImpl(): Boolean {
        return if (view is AnimatedViewHolder) {
            view.preAnimateRemoveImpl()
        } else false
    }

    override fun animateAddImpl(listener: ViewPropertyAnimatorListener?, addDuration: Long, index: Int): Boolean {
        return if (view is AnimatedViewHolder) {
            view.animateAddImpl(listener, addDuration, index)
        } else false
    }

    override fun animateRemoveImpl(listener: ViewPropertyAnimatorListener?, removeDuration: Long, index: Int): Boolean {
        return if (view is AnimatedViewHolder) {
            view.animateRemoveImpl(listener, removeDuration, index)
        } else false
    }

    val view: V = view as V
    private var listener: InteractiveModelView.Listener? = null

    fun setListener(listener: InteractiveModelView.Listener?) {
        this.listener = listener
        if (listener != null && view is InteractiveModelView<*>) {
            (view as InteractiveModelView<*>).listener = listener
        }
    }
}

class StaticFlexibleViewHolder : FlexibleViewHolder {
    constructor(view: View?, adapter: FlexibleAdapter<out IFlexible<*>>?) : super(view, adapter)
    constructor(view: View?, adapter: FlexibleAdapter<out IFlexible<*>>?, stickyHeader: Boolean) : super(view, adapter, stickyHeader)
}
