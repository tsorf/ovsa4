package ru.frosteye.ovsa.presentation.view.tool;

import org.jetbrains.annotations.NotNull;

import ru.frosteye.ovsa.presentation.view.contract.IPrompt;



public class SimplePrompt implements IPrompt {

    private String title;
    private String hint;
    private String negativeButton;
    private String positiveButton;
    private String text;
    private int maxLength;
    private boolean cancelable = true;
    private int inputType;

    public SimplePrompt setTitle(String title) {
        this.title = title;
        return this;
    }

    public SimplePrompt setHint(String hint) {
        this.hint = hint;
        return this;
    }

    public SimplePrompt setText(String text) {
        this.text = text;
        return this;
    }

    public SimplePrompt setNegativeButton(String negativeButton) {
        this.negativeButton = negativeButton;
        return this;
    }

    public boolean cancelable() {
        return cancelable;
    }

    public SimplePrompt setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public SimplePrompt setPositiveButton(String positiveButton) {
        this.positiveButton = positiveButton;
        return this;
    }

    public SimplePrompt setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        return this;
    }

    public SimplePrompt setInputType(int inputType) {
        this.inputType = inputType;
        return this;
    }

    @Override
    public String title() {
        return title;
    }

    @NotNull
    @Override
    public String text() {
        return text;
    }

    @Override
    public String hint() {
        return hint;
    }

    @Override
    public String positiveButton() {
        return positiveButton;
    }

    @Override
    public String negativeButton() {
        return negativeButton;
    }

    @Override
    public int maxLength() {
        return maxLength;
    }

    @Override
    public int inputType() {
        return inputType;
    }
}
