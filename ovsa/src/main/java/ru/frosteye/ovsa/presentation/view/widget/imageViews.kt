package ru.frosteye.ovsa.presentation.view.widget

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import ru.frosteye.ovsa.R
import java.io.File




fun ImageView.makeRound(): ImageView {
    try {
        val original = (drawable as BitmapDrawable).bitmap
        Glide.with(context).load(original)
            .apply(
                RequestOptions.circleCropTransform()
            )
            .into(this)
    } catch (e: Exception) {
        Log.w("RoundedImageView", "Drawable is not bitmap drawable")
    }
    return this
}

fun ImageView.makeRound(radius: Float): ImageView {
    try {
        val original = (drawable as BitmapDrawable).bitmap;
        val drawable = RoundedBitmapDrawableFactory.create(context.resources, original)
        drawable.cornerRadius = radius
        setImageDrawable(drawable)
    } catch (e: Exception) {
        Log.w("RoundedImageView", "Drawable is not bitmap drawable")
    }
    return this
}

fun ImageView.loadImage(file: File?, options: RequestOptions? = null) {
    if (file == null) {
        this.setImageDrawable(null)
    } else {
        Glide.with(context).load(file).apply {
            options?.let { apply(it) }
            into(this@loadImage)
        }
    }
}


fun ImageView.loadImage(url: String?) {
    if (url == null) {
        this.setImageDrawable(null)
    } else {
        Glide.with(context).load(url)
            .apply(
                RequestOptions()
                    .centerCrop()
            ).into(this)
    }
}

fun ImageView.loadImage(url: String?, @DrawableRes default: Int) {
    if (url == null) {
        this.setImageResource(default)
    } else {
        Glide.with(context).load(url)
            .apply(
                RequestOptions()
                    .centerCrop()
            ).into(this)
    }
}

fun ImageView.loadImage(url: String?, requestOptions: RequestOptions) {
    if (url == null) {
        setImageDrawable(null)
    } else {
        Glide.with(context).load(url)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}

fun ImageView.loadImage(
    url: String?, @DrawableRes default: Int,
    requestOptions: RequestOptions,
    complete: ((Drawable) -> Unit)? = null,
    error: ((java.lang.Exception) -> Unit)? = null
) {
    if (url == null) {
        Glide.with(context).load(default)
            .apply(requestOptions)
            .into(this)
    } else {
        val builder = Glide.with(context).load(url)
        if (complete != null || error != null) {
            builder.listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    e?.let {
                        error?.invoke(it)
                    }
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    complete?.invoke(resource)
                    return false
                }

            })
        }
        builder
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}


fun View.loadBackground(url: String?, requestOptions: RequestOptions) {
    if (url == null) {
        background = null
    } else {
        Glide.with(context).load(url)
            .apply(requestOptions)
            .into(object : SimpleTarget<Drawable>() {
                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                    background = resource
                }

            })
    }
}

fun ImageView.loadImage(
    url: String?, @DrawableRes default: Int, @DrawableRes placeholder: Int,
    requestOptions: RequestOptions
) {
    if (url == null) {
        Glide.with(context).load(default)
            .apply(requestOptions)
            .into(this)
    } else {
        Glide.with(context)
            .load(url)
            .apply(requestOptions.placeholder(placeholder))
            .into(this)
    }
}

fun ImageView.loadAndMakeRound(url: String?, @DrawableRes default: Int, radius: Int) {
    if (url == null) {
        Glide.with(context).load(default)
            .apply(
                RequestOptions.bitmapTransform(
                    RoundedCorners(radius)
                )
            )
            .into(this)
    } else {
        Glide.with(context).load(url)
            .apply(
                RequestOptions.bitmapTransform(
                    RoundedCorners(radius)
                )
            )
            .into(this)
    }
}

fun ImageView.loadRawImage(url: String?) {
    if (url == null) {
        this.setImageDrawable(null)
    } else {
        Glide.with(context).load(url).into(this)
    }
}

fun ImageView.loadFullImageWithCallback(
    url: String?,
    success: (Bitmap) -> Unit,
    error: () -> Unit
) {
    if (url == null) {
        this.setImageDrawable(null)
    } else {
        val target = object : SimpleTarget<Bitmap>() {

            override fun onLoadFailed(errorDrawable: Drawable?) {
                super.onLoadFailed(errorDrawable)
                error.invoke()
            }

            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                setImageBitmap(resource)
                success.invoke(resource)
            }
        }
        Glide.with(context)
            .asBitmap()
            .load(url)
            .into(target)
    }
}

fun ImageView.loadAndMakeRound(url: String?) {
    if (url == null) {
        this.setImageDrawable(null)
    } else {
        Glide.with(context).load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(this)
    }
}

class RatioImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseImageView(context, attrs, defStyleAttr) {

    private var ratioWidth: Int = 1
    private var ratioHeight: Int = 1

    override fun onTypedArrayReady(array: TypedArray) {
        ratioWidth = array.getInteger(R.styleable.RatioImageView_ratioWidth, 1)
        ratioHeight = array.getInteger(R.styleable.RatioImageView_ratioHeight, 1)
    }

    override fun prepareView() {

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val originalWidth = View.MeasureSpec.getSize(widthMeasureSpec)

        val originalHeight = View.MeasureSpec.getSize(heightMeasureSpec)

        val calculatedHeight = originalWidth * ratioWidth / ratioHeight

        val finalWidth: Int
        val finalHeight: Int

        if (calculatedHeight > originalHeight) {
            finalWidth = originalHeight * ratioWidth / ratioHeight
            finalHeight = originalHeight
        } else {
            finalWidth = originalWidth
            finalHeight = calculatedHeight
        }

        super.onMeasure(
            View.MeasureSpec.makeMeasureSpec(finalWidth, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(finalHeight, View.MeasureSpec.EXACTLY)
        )
    }
}

class HeartBeatImageView : BaseImageView {

    private var objectAnimator: ObjectAnimator? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun prepareView() {
        objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
            this,
            PropertyValuesHolder.ofFloat("scaleX", 1.2f),
            PropertyValuesHolder.ofFloat("scaleY", 1.2f)
        )
        objectAnimator!!.duration = DEFAULT_DURATION.toLong()

        objectAnimator!!.repeatCount = ObjectAnimator.INFINITE
        objectAnimator!!.repeatMode = ObjectAnimator.REVERSE
    }

    fun startBeating() {
        objectAnimator!!.start()
    }

    fun stopBeating() {
        objectAnimator!!.cancel()
    }

    fun boost() {
        objectAnimator!!.duration = BOOST_DURATION.toLong()
    }

    fun calmDown() {
        objectAnimator!!.duration = DEFAULT_DURATION.toLong()
    }

    companion object {

        val DEFAULT_DURATION = 700
        val BOOST_DURATION = 200
    }
}