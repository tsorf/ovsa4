package ru.frosteye.ovsa.presentation.view.contract



interface IPrompt {
    fun title(): String
    fun hint(): String
    fun text(): String?
    fun positiveButton(): String
    fun negativeButton(): String
    fun inputType(): Int
    fun maxLength(): Int
    fun cancelable(): Boolean

    interface Listener {
        fun onResult(text: String)
        fun onCancel()
    }
}
