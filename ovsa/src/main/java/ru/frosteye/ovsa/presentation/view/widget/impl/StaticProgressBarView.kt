package ru.frosteye.ovsa.presentation.view.widget.impl

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.os.Build
import androidx.annotation.RequiresApi
import android.util.AttributeSet
import android.view.View

import ru.frosteye.ovsa.R




class StaticProgressBarView : View {

    private var progress = 0
    private var borderPaint: Paint? = null
    private var filledPaint: Paint? = null
    private var borderPath: Path? = null
    private var fillPath: Path? = null
    private var maskPath: Path? = null
    private var maskPaint: Paint? = null
    private var arc: RectF? = null
    private var fillColor: Int = 0
    private var borderColor: Int = 0
    private var maskColor: Int = 0
    private var cornerRadius = 5f
    private var borderWidth = 3f


    constructor(context: Context) : super(context) {
        init(context.obtainStyledAttributes(R.styleable.StaticProgressBarView))
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context.obtainStyledAttributes(attrs, R.styleable.StaticProgressBarView))
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context.obtainStyledAttributes(attrs, R.styleable.StaticProgressBarView, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context.obtainStyledAttributes(attrs, R.styleable.StaticProgressBarView, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        if (array.hasValue(R.styleable.StaticProgressBarView_initialProgress)) {
            progress = array.getInt(R.styleable.StaticProgressBarView_initialProgress, 0)
        }
        if (array.hasValue(R.styleable.StaticProgressBarView_barFillColor)) {
            fillColor = array.getColor(R.styleable.StaticProgressBarView_barFillColor, 0)
        }
        if (array.hasValue(R.styleable.StaticProgressBarView_maskColor)) {
            maskColor = array.getColor(R.styleable.StaticProgressBarView_maskColor, 0)
        }
        if (array.hasValue(R.styleable.StaticProgressBarView_barBorderColor)) {
            borderColor = array.getColor(R.styleable.StaticProgressBarView_barBorderColor, 0)
        }
        if (array.hasValue(R.styleable.StaticProgressBarView_barBorderWidth)) {
            borderWidth = array.getDimension(R.styleable.StaticProgressBarView_barBorderWidth, 0f)
        }
        if (array.hasValue(R.styleable.StaticProgressBarView_barCornerRadius)) {
            cornerRadius = array.getDimension(R.styleable.StaticProgressBarView_barCornerRadius, 5f)
        }

        filledPaint = Paint()
        if (fillColor != 0) {
            filledPaint!!.color = fillColor
        } else {
            filledPaint!!.color = Color.GRAY
        }
        filledPaint!!.isAntiAlias = true
        filledPaint!!.style = Paint.Style.FILL

        maskPaint = Paint()
        if (maskColor != 0) {
            maskPaint!!.color = maskColor
        } else {
            maskPaint!!.color = Color.WHITE
        }
        maskPaint!!.isAntiAlias = true
        maskPaint!!.style = Paint.Style.FILL

        borderPaint = Paint()
        if (borderColor != 0) {
            borderPaint!!.color = borderColor
        } else {
            borderPaint!!.color = Color.LTGRAY
        }
        borderPaint!!.isAntiAlias = true
        borderPaint!!.style = Paint.Style.STROKE
        borderPaint!!.strokeWidth = borderWidth

        borderPath = Path()
        fillPath = Path()
        maskPath = Path()
        arc = RectF()
    }

    override fun onDraw(canvas: Canvas) {
        drawProgress(canvas)
    }

    fun setProgress(progress: Int) {
        this.progress = progress
        if (this.progress > 100) this.progress = 100
        if (this.progress < 0) this.progress = 0
        invalidate()
    }

    private fun drawProgress(canvas: Canvas) {
        val width = measuredWidth
        val height = measuredHeight
        val leftTopX = PADDING + borderWidth / 2
        val leftTopY = (height - height / 3).toFloat()
        val leftBottomX = PADDING + borderWidth / 2
        val leftBottomY = height.toFloat() - PADDING.toFloat() - borderWidth / 2
        //        if((height - leftTopY) / 2 < cornerRadius)
        //            cornerRadius = (height - leftTopY) / 2;
        val rightTopX = width.toFloat() - PADDING.toFloat() - borderWidth / 2
        val rightTopY = PADDING.toFloat()
        val rightBottomX = width.toFloat() - PADDING.toFloat() - borderWidth / 2
        val rightBottomY = height.toFloat() - PADDING.toFloat() - borderWidth / 2

        val progressXEdge = (width / 100 * progress).toFloat()

        if (progress != 0) {
            fillPath!!.reset()
            fillPath!!.moveTo(PADDING.toFloat(), PADDING.toFloat())
            fillPath!!.lineTo(PADDING.toFloat(), (height - PADDING).toFloat())
            fillPath!!.lineTo(progressXEdge, (height - PADDING).toFloat())
            fillPath!!.lineTo(progressXEdge, PADDING.toFloat())
            fillPath!!.close()
            canvas.drawPath(fillPath!!, filledPaint!!)
        }

        maskPath!!.reset()
        maskPath!!.moveTo(PADDING.toFloat(), PADDING.toFloat())
        maskPath!!.lineTo(PADDING.toFloat(), leftTopY)
        maskPath!!.lineTo(rightTopX, PADDING.toFloat())
        maskPath!!.close()
        canvas.drawPath(maskPath!!, maskPaint!!)

        borderPath!!.reset()
        borderPath!!.moveTo(leftTopX, leftTopY + cornerRadius)
        borderPath!!.lineTo(leftBottomX, leftBottomY - cornerRadius)
        borderPath!!.lineTo(rightBottomX - cornerRadius, rightBottomY)

        //        arc.set(leftBottomX, leftBottomY - cornerRadius, leftBottomX + cornerRadius, leftBottomY);
        //        borderPath.arcTo(arc, 180, 270);

        borderPath!!.lineTo(rightTopX, rightTopY + cornerRadius)

        borderPath!!.lineTo(rightTopX, rightTopY + cornerRadius)

        borderPath!!.lineTo(leftTopX + cornerRadius, leftTopY)

        borderPath!!.close()
        canvas.drawPath(borderPath!!, borderPaint!!)
    }

    companion object {
        val PADDING = 0
    }
}



