package ru.frosteye.ovsa.presentation.presenter


interface Presenter<T> {
    fun onAttach(v: T, vararg params: Any)
}
