package ru.frosteye.ovsa.data.storage


import ru.frosteye.ovsa.execution.serialization.Serializer

interface Storage {
    fun save(key: String, payload: Any?)
    fun <T> get(key: String, clazz: Class<T>): T?

    fun serializer(): Serializer
}
