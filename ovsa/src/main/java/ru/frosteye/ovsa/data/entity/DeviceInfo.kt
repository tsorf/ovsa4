package ru.frosteye.ovsa.data.entity

import com.google.gson.annotations.SerializedName


data class DeviceInfo(
    @SerializedName("id") val deviceId: String,
    @SerializedName("name") val name: String,
    @SerializedName("manufacturer") val manufacturer: String,
    @SerializedName("osVersion") val osVersion: String,
    @SerializedName("os") val os: String = "android"
)