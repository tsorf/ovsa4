package ru.frosteye.ovsa.data.storage

import android.content.Context
import android.os.Environment
import ru.frosteye.ovsa.execution.serialization.Serializer
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DistributedStorage @Inject constructor(
    private val context: Context,
    private val serializer: Serializer
) : Storage {

    private val rootDirectory: File =
        File(Environment.getDataDirectory(), "distributedStorage")

    init {
        if (!rootDirectory.exists()) {
            rootDirectory.mkdir()
        }
    }

    override fun save(key: String, payload: Any?) {
        var model = payload
        if (isPrimitive(model)) {
            model = payload.toString()
        }
        saveInternal(key, model)
    }

    @Synchronized
    private fun saveInternal(key: String, payload: Any?) {
        File(rootDirectory, key).apply {
            if (payload != null) {
                if (!exists()) {
                    createNewFile()
                }
                writeText(serializer.serialize(payload))
            } else {
                if (exists()) {
                    delete()
                }
            }

        }
    }

    @Synchronized
    private fun loadInternal(key: String): String? {
        File(rootDirectory, key).apply {
            if (!exists()) return null
            return readText()
        }
    }

    override fun <T> get(key: String, clazz: Class<T>): T? {
        return loadInternal(key)?.let {
            serializer.deserialize(it, clazz)
        }
    }

    override fun serializer(): Serializer {
        return serializer
    }

    companion object {

        private val primitives: Set<Class<*>> = setOf(
            Boolean::class.java,
            Char::class.java,
            Byte::class.java,
            Short::class.java,
            Int::class.java,
            Long::class.java,
            Float::class.java,
            Double::class.java,
            Void::class.java
        )

        private fun isPrimitive(payload: Any?): Boolean {
            return if (payload == null) false else primitives.contains(payload.javaClass)
        }
    }
}