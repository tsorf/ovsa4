package ru.frosteye.ovsa.data.entity

import android.location.Location
import com.google.gson.annotations.SerializedName

import java.io.Serializable



open class SimpleLocation(
        @SerializedName("lat") val lat: Double,
        @SerializedName("lng") val lng: Double
) : Serializable {

    constructor(location: Location) : this(location.latitude, location.longitude)
}
