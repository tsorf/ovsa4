package ru.frosteye.ovsa.data.storage

import io.reactivex.Observable
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


@Deprecated("just do not use it")
interface CommonRepository : Repository<RepoIndex>

interface Repository<T> : ReadWriteProperty<Any?, T?> {

    fun load(): Observable<T>

    fun save(data: T?)

    fun executeAndSave(runnable: (T) -> Unit)

    fun addListener(listener: Listener<T>)

    fun removeListener(listener: Listener<T>)

    interface Listener<T> {

        fun onUpdate(data: T?)
    }
}

@Deprecated("just do not use it")
class RepoIndex {
    var state: Int = 0
}

@Deprecated("not proguard safe & shitty design")
abstract class IndexRepository(storage: Storage) : BaseRepository<RepoIndex>(storage) {

    override val clazz: Class<RepoIndex> = RepoIndex::class.java

    override fun handleNull(): RepoIndex? = RepoIndex()

    final override fun save(data: RepoIndex?) {
        throw RuntimeException("index rewriting is not allowed in IndexRepository")
    }
}

@Deprecated("no proguard support")
abstract class BaseRepository<T>(protected var storage: Storage) : Repository<T> {

    protected var model: T? = null
    protected abstract val clazz: Class<T>
    private val listeners: MutableSet<Repository.Listener<T>> = mutableSetOf()

    @Synchronized
    override fun load(): Observable<T> {
        return createSource()
    }

    protected open fun loadLocal(): T? {
        if (model == null) {
            model = storage.get(clazz.name, clazz)
        }
        if (model == null) {
            model = handleNull()
        }
        return model
    }

    @Synchronized
    override fun executeAndSave(runnable: (T) -> Unit) {
        val data = loadLocal()
        data?.let(runnable)
        save(data)
    }

    protected open fun createSource(): Observable<T> {
        val data = loadLocal()
        return Observable.just(data)
    }

    protected open fun handleNull(): T? {
        return null
    }

    @Synchronized
    override fun save(data: T?) {
        this.model = data
        storage.save(clazz.name, model)
        synchronized(listeners) {
            listeners.forEach { it.onUpdate(data) }
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return loadLocal()
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        save(value)
    }

    override fun addListener(listener: Repository.Listener<T>) {
        synchronized(listeners) {
            listeners.add(listener)
        }
    }

    override fun removeListener(listener: Repository.Listener<T>) {
        synchronized(listeners) {
            listeners.remove(listener)
        }
    }
}


@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Persisted(
    val storageKey: String
)

abstract class KeyValueRepository<T>(
    protected var storage: Storage
) : Repository<T> {

    protected var model: T? = null
    protected abstract val clazz: Class<T>
    private val listeners: MutableSet<Repository.Listener<T>> = mutableSetOf()

    @Synchronized
    override fun load(): Observable<T> {
        return createSource()
    }

    protected open fun loadLocal(): T? {
        if (model == null) {
            model = storage.get(getStorageKey(), clazz)
        }
        if (model == null) {
            model = handleNull()
        }
        return model
    }

    @Synchronized
    override fun executeAndSave(runnable: (T) -> Unit) {
        val data = loadLocal()
        data?.let(runnable) ?: throw IllegalArgumentException("trying to executeAndSave on null repo value")
        save(data)
    }

    protected open fun createSource(): Observable<T> {
        return loadLocal()?.let {
            Observable.just(it)
        } ?: run {
//            Observable.error(IllegalStateException("${getStorageKey()} is empty"))
            Observable.empty<T>()
        }
    }

    protected open fun handleNull(): T? {
        return null
    }

    @Synchronized
    override fun save(data: T?) {
        this.model = data
        storage.save(getStorageKey(), model)
        synchronized(listeners) {
            listeners.forEach { it.onUpdate(data) }
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return loadLocal()
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        save(value)
    }

    override fun addListener(listener: Repository.Listener<T>) {
        synchronized(listeners) {
            listeners.add(listener)
        }
    }

    override fun removeListener(listener: Repository.Listener<T>) {
        synchronized(listeners) {
            listeners.remove(listener)
        }
    }

    private fun getStorageKey(): String {
        return AnnotationHelper.getStorageKey(clazz)
    }
}
