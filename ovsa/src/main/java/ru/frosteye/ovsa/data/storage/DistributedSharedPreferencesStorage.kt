package ru.frosteye.ovsa.data.storage

import android.content.Context
import android.content.SharedPreferences

import java.util.HashSet

import javax.inject.Inject

import ru.frosteye.ovsa.execution.serialization.Serializer
import ru.frosteye.ovsa.tool.common.md5

class DistributedSharedPreferencesStorage @Inject
constructor(
    private val context: Context,
    private val serializer: Serializer
) : Storage {

    private val dataKey: String = "data"

    override fun save(key: String, payload: Any?) {
        var model = payload
        if (isPrimitive(model)) {
            model = payload.toString()
        }
        put(key, model)
    }



    private fun put(key: String, model: Any?) {
        getPreferences(key)
            .edit()
            .putString(
                dataKey, if (model != null)
                    serializer.serialize(model) else null
            )
            .apply()
    }

    private fun getPreferences(key: String): SharedPreferences {
        return context.getSharedPreferences(key.md5(), Context.MODE_PRIVATE)
    }

    override fun <T> get(key: String, clazz: Class<T>): T? {
        return serializer
            .deserialize(getPreferences(key).getString(dataKey, null), clazz)
    }

    override fun serializer(): Serializer {
        return serializer
    }

    companion object {

        private val primitives: Set<Class<*>> = setOf(
            Boolean::class.java,
            Char::class.java,
            Byte::class.java,
            Short::class.java,
            Int::class.java,
            Long::class.java,
            Float::class.java,
            Double::class.java,
            Void::class.java
        )

        private fun isPrimitive(payload: Any?): Boolean {
            return if (payload == null) false else primitives.contains(payload.javaClass)
        }
    }
}
