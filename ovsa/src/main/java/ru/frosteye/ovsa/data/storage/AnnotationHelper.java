package ru.frosteye.ovsa.data.storage;

import java.lang.annotation.Annotation;

public class AnnotationHelper {

    public static String getStorageKey(Class<?> clazz) {
        if (clazz.isAnnotationPresent(Persisted.class)) {
            return clazz.getAnnotation(Persisted.class).storageKey();
        }
        throw new IllegalArgumentException(clazz + " is not persisted");
    }
}
