package ru.frosteye.ovsa.data.storage

import android.content.SharedPreferences

import java.util.HashSet

import javax.inject.Inject

import ru.frosteye.ovsa.execution.serialization.Serializer

@Deprecated("use DistributedSharedPreferencesStorage")
class SharedPreferencesStorage @Inject
constructor(private val sharedPreferences: SharedPreferences, private val serializer: Serializer) :
    Storage {

    override fun save(key: String, payload: Any?) {
        var model = payload
        if (isPrimitive(model)) {
            model = payload.toString()
        }
        sharedPreferences.edit().putString(
            key,
            if (model != null) serializer.serialize(model) else null
        ).apply()
    }

    override fun <T> get(key: String, clazz: Class<T>): T? {
        return serializer.deserialize(sharedPreferences.getString(key, null), clazz)
    }

    override fun serializer(): Serializer {
        return serializer
    }

    companion object {

        private val primitives: Set<Class<*>> = setOf(
            Boolean::class.java,
            Char::class.java,
            Byte::class.java,
            Short::class.java,
            Int::class.java,
            Long::class.java,
            Float::class.java,
            Double::class.java,
            Void::class.java
        )

        private fun isPrimitive(payload: Any?): Boolean {
            return if (payload == null) false else primitives.contains(payload.javaClass)
        }
    }
}
