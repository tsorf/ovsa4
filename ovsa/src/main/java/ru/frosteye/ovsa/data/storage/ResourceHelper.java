package ru.frosteye.ovsa.data.storage;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.ColorRes;
import androidx.annotation.IntegerRes;
import androidx.annotation.StringRes;

import android.util.DisplayMetrics;
import android.util.TypedValue;


@Deprecated
public class ResourceHelper {

    private static ResourceHelper instance;

    private Resources resources;
    private Context appContext;
    private DisplayMetrics displayMetrics;

    public static void init(Context context) {
        instance = new ResourceHelper(context);
    }

    private ResourceHelper(Context context) {
        appContext = context.getApplicationContext();
        resources = context.getApplicationContext().getResources();
        displayMetrics = context.getResources().getDisplayMetrics();

    }


    public static int getStatusBarHeight() {
        int result = 0;
        int resourceId = instance.resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = instance.resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Deprecated
    public static String getString(@StringRes int res) {
        return instance.resources.getString(res);
    }

    @Deprecated
    public static String getString(@StringRes int res, Object... args) {
        return instance.resources.getString(res, args);
    }

    public static String s(@StringRes int res) {
        return instance.resources.getString(res);
    }

    public static String s(@StringRes int res, Object... args) {
        return instance.resources.getString(res, args);
    }

    public static Resources getResources() {
        return instance.resources;
    }

    public static int getInteger(@IntegerRes int res) {
        return instance.resources.getInteger(res);
    }

    public static int getColor(@ColorRes int res) {
        return instance.resources.getColor(res);
    }

    public static Context getAppContext() {
        return instance.appContext;
    }

    public static DisplayMetrics displayMetrics() {
        return instance.appContext.getResources().getDisplayMetrics();
    }

    public static float dpToPx(float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, instance.displayMetrics);
    }
}
