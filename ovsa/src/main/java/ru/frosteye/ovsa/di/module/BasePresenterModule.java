package ru.frosteye.ovsa.di.module;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import androidx.fragment.app.Fragment;



public abstract class BasePresenterModule<A extends AppCompatActivity, F extends Fragment> {

    private A activity;
    private View view;
    private F fragment;

    public BasePresenterModule(View view) {
        this.view = view;
    }

    public BasePresenterModule(A activity) {
        this.activity = activity;
    }

    public BasePresenterModule(F fragment) {
        this.fragment = fragment;
    }

    public BasePresenterModule() {

    }

    protected Context getContext() {
        if(activity != null) return activity;
        if(fragment != null) return fragment.getContext();
        if(view != null) return view.getContext();
        throw new IllegalStateException("Presenter module is not initialized");
    }
}
