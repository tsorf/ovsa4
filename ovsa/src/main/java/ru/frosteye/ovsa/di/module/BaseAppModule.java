package ru.frosteye.ovsa.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.WindowManager;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;
import ru.frosteye.ovsa.R;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.data.storage.SharedPreferencesStorage;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.ovsa.di.qualifer.AndroidId;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.executor.ThreadExecutor;
import ru.frosteye.ovsa.execution.executor.UIThread;
import ru.frosteye.ovsa.execution.network.client.FixedKladrClient;
import ru.frosteye.ovsa.execution.serialization.GsonSerializer;
import ru.frosteye.ovsa.execution.serialization.Serializer;



@Module
public abstract class BaseAppModule<T extends Context> {

    protected T context;

    public BaseAppModule(T context) {
        this.context = context;
        ResourceHelper.init(context);
        RxJavaPlugins.setErrorHandler(Functions.<Throwable>emptyConsumer());
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return context;
    }

    @Provides @Singleton
    public MainThread provideMainThread() {
        return new UIThread();
    }

    @Provides @Singleton
    public Executor provideExecutor(ThreadExecutor executor) {
        return executor;
    }

    @Provides @Singleton
    public PowerManager providePowerManager() {
        return ((PowerManager) context.getSystemService(Context.POWER_SERVICE));
    }

    @Provides @Singleton
    public AudioManager provideAudioManager() {
        return ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE));
    }

    @Provides @Singleton
    public LocationManager provideLocationManager() {
        return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE));
    }

    @Provides @Singleton
    public WindowManager provideWindowManager() {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
    }

    @Provides @Singleton
    public SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(provideDefaultPreferencesName(), Context.MODE_PRIVATE);
    }

    @Provides @Singleton
    public FixedKladrClient provideKladrClient(Context context) {
        return new FixedKladrClient(context.getString(R.string.kladr_token), context.getString(R.string.kladr_key));
    }

    @AndroidId
    @Provides @Singleton
    public String provideAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    protected String provideDefaultPreferencesName() {
        return context.getPackageName();
    };
}
