package ru.frosteye.ovsa.di.qualifer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;


@Qualifier
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
public @interface AndroidId {
}
