package ru.frosteye.ovsa.execution.executor

import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import javax.inject.Inject



class ThreadExecutor @Inject
constructor() : Executor {
    private val threadPoolExecutor: ExecutorService = Executors.newFixedThreadPool(8)

    override fun execute(command: Runnable) {
        threadPoolExecutor.execute(command)
    }
}
