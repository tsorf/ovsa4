package ru.frosteye.ovsa.execution.serialization


interface Serializer {
    /**
     * Perform serialization mechanism on source [Object]
     * @param any [Object] to serialize
     * @return [String], representing source object;
     */
    fun serialize(any: Any?): String

    /**
     * Perform deserialization mechanism on source [String] instance.
     * @param string [String] to deserialize
     * @param typeOfT resulting object's type
     * @param <T> any type implementation able to deserialize
     * @return deserialized object
    </T> */
    fun <T> deserialize(string: String?, typeOfT: Class<T>): T
}
