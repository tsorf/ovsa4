package ru.frosteye.ovsa.execution.serialization

import com.google.gson.*
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class DateSerializer(
    private val hard: Boolean
) : JsonDeserializer<Date>, JsonSerializer<Date> {

    private val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).apply {
        timeZone = TimeZone.getTimeZone("UTC")
    }
    private val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).apply {
        timeZone = TimeZone.getDefault()
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Date? {
        if (json == null || json is JsonNull) return null
        try {
            val string = json.asString
            val result = parser.parse(string)
            return result

        } catch (e: Exception) {
            e.printStackTrace()
            if (hard) throw e
            else return Date()
        }
    }

    override fun serialize(
        src: Date?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        if (src == null) return JsonNull.INSTANCE
        return JsonPrimitive(formatter.format(src))

    }
}