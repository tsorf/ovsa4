package ru.frosteye.ovsa.execution.task

import java.util.concurrent.Executor

import javax.inject.Inject

import io.reactivex.Observable
import ru.frosteye.ovsa.execution.executor.MainThread
import ru.frosteye.ovsa.execution.network.client.FixedKladrClient



class LoadCitiesTask @Inject
constructor(mainThread: MainThread,
            executor: Executor,
            private val fixedKladrClient: FixedKladrClient)
    : ObservableTask<String, List<String>>(mainThread, executor) {
    private var limit = 3

    fun setLimit(limit: Int) {
        this.limit = limit
    }

    override fun prepareObservable(string: String): Observable<List<String>> {
        return Observable.create { subscriber ->
            try {
                val cities = fixedKladrClient.getCityNames(string, limit)
                subscriber.onNext(cities)
                subscriber.onComplete()
            } catch (e: Exception) {
                subscriber.onError(e)
            }
        }
    }
}
