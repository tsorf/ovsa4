package ru.frosteye.ovsa.execution.task

import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.frosteye.ovsa.BuildConfig
import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.execution.network.base.ApiException
import ru.frosteye.ovsa.presentation.view.contract.ConsumerView
import ru.frosteye.ovsa.tool.s
import java.util.concurrent.Executor
import java.util.concurrent.Executors


private val threadPoolExecutor: Executor = Executors.newFixedThreadPool(8)
val GSON = Gson()

@Deprecated("use toback() & toFront()")
fun <T : Any> Observable<T>.async(): Observable<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
            .materialize()
            .observeOn(AndroidSchedulers.mainThread())
            .dematerialize()
}

fun <T : Any> Observable<T>.toBack(): Observable<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun <T : Any> Maybe<T>.toBack(): Maybe<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun Completable.toBack(): Completable {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun <T : Any> Observable<T>.toFront(): Observable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun <T : Any> Maybe<T>.toFront(): Maybe<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun Completable.toFront(): Completable {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun <T : Any> Single<T>.toBack(): Single<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun <T : Any> Single<T>.toFront(): Single<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

object ApiExceptionInterloper {

    private val listeners: MutableSet<Listener> = mutableSetOf()

    fun addListener(listener: Listener) {
        synchronized(listeners) {
            listeners.add(listener)
        }
    }
    fun removeListener(listener: Listener) {
        synchronized(listeners) {
            listeners.remove(listener)
        }
    }

    fun onApiException(e: ApiException) {
        synchronized(listeners) {
            listeners.forEach {
                it.onApiException(e)
            }
        }
    }

    interface Listener {
        fun onApiException(e: ApiException)
    }
}

fun <T : Any> Observable<T>.request(result: (T) -> Unit, error: (Throwable) -> Unit): Disposable {
    return this
            .toFront()
            .subscribe({
                result.invoke(it)
            }, {
                try {
                    if (it is HttpException) {
                        val resp = GSON.fromJson(
                                it.response()?.errorBody()?.string(), ru.frosteye.ovsa.execution.network.response.YiiMessageResponse::class.java
                        )

                        error.invoke(ApiException(
                                resp.message, resp.code
                        ).apply {
                            ApiExceptionInterloper.onApiException(this)
                        })
                    } else {
                        it.printStackTrace()
                        error.invoke(ApiException(
                            createDefaultErrorMessage(it), 0
                        ).apply {
                            ApiExceptionInterloper.onApiException(this)
                        })
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    error.invoke(ApiException(
                            createDefaultErrorMessage(e), -1
                    ).apply {
                        ApiExceptionInterloper.onApiException(this)
                    })
                }

            })
}

fun <T : Any> Single<T>.request(result: (T) -> Unit, error: (Throwable) -> Unit = {

}): Disposable {
    return this
            .toFront()
            .subscribe({
                result.invoke(it)
            }, {exception ->
                var it: Throwable = exception
                if (exception is CompositeException) {
                    it = exception.exceptions.firstOrNull() ?: exception
                }
                try {
                    if (it is HttpException) {
                        val resp = GSON.fromJson(
                                it.response()?.errorBody()?.string(), ru.frosteye.ovsa.execution.network.response.YiiMessageResponse::class.java
                        )
                        error.invoke(ApiException(
                                resp.message ?: createDefaultErrorMessage(it), resp.code
                        ).apply {
                            ApiExceptionInterloper.onApiException(this)
                        })
                    } else {
                        it.printStackTrace()
                        error.invoke(ApiException(
                            createDefaultErrorMessage(it), 0
                        ).apply {
                            ApiExceptionInterloper.onApiException(this)
                        })
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    error.invoke(ApiException(
                        createDefaultErrorMessage(e), -1
                    ).apply {
                        ApiExceptionInterloper.onApiException(this)
                    })
                }

            })
}

private fun createDefaultErrorMessage(e: Throwable): String {
    return if (BuildConfig.DEBUG) {
        e.message ?: R.string.default_connection_error.s()
    } else {
        R.string.default_connection_error.s()
    }
}

@Deprecated("unused junk")
fun <T : Any> Observable<T>.toView(view: ConsumerView<T>, enableControls: Boolean = true): Disposable {
    return this
            .toFront()
            .subscribe({
                if (enableControls)
                    view.enableControls(true, 0)
                view.model = it
            }, {
                try {
                    if (it is HttpException) {
                        val resp = GSON.fromJson(
                                it.response()?.errorBody()?.string(), ru.frosteye.ovsa.execution.network.response.YiiMessageResponse::class.java
                        )
                        val ex = ApiException(
                                resp.message, resp.code
                        )
                        if (enableControls)
                            view.enableControls(true, -1)
                        view.showMessage(ex.message, -1)
                    } else {
                        it.printStackTrace()
                        val ex = ApiException(
                                R.string.default_connection_error.s(), 0
                        )
                        if (enableControls)
                            view.enableControls(true, -1)
                        view.showMessage(ex.message, -1)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    val ex = ApiException(
                            R.string.default_connection_error.s(), 0
                    )
                    if (enableControls)
                        view.enableControls(true, -1)
                    view.showMessage(ex.message, -1)
                }

            })
}

