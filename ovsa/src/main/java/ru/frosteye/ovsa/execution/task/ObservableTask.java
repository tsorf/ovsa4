package ru.frosteye.ovsa.execution.task;

import java.util.concurrent.Executor;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ru.frosteye.ovsa.execution.executor.MainThread;


/**
 * The base worker class, that represents any Observable-based operations
 * @param <Params> input param's type
 * @param <Result> the result of this task
 */
@Deprecated
public abstract class ObservableTask<Params, Result> {
    private MainThread mainThread;
    private Executor executor;
    private Disposable disposable;
    private Params params;

    public ObservableTask(MainThread mainThread, Executor executor) {
        this.mainThread = mainThread;
        this.executor = executor;
    }

    protected abstract Observable<Result> prepareObservable(Params params);

    public void cancel() {
        if(disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
    }

    public void execute(Params params, Consumer<Result> resultConsumer, Consumer<Throwable> excConsumer) {
        disposable = createObservable(params).subscribe(resultConsumer, excConsumer);
    }

    public void execute(Params params, final Observer<Result> subscriber) {
        createObservable(params).subscribe(new Observer<Result>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(Result result) {
                subscriber.onNext(result);
            }

            @Override
            public void onError(Throwable e) {
                subscriber.onError(e);
            }

            @Override
            public void onComplete() {
                subscriber.onComplete();
            }
        });
    }

    private Observable<Result> createObservable(Params params){
        cancel();
        this.params = params;
        return prepareObservable(params)
                .subscribeOn(getExecutionScheduler())
                .materialize()
                .observeOn(getObserveScheduler())
                .dematerialize();
    }

    public Params getParams() {
        return params;
    }

    public void execute(Params params) {
        disposable = createObservable(params).subscribe();
    }

    protected Scheduler getExecutionScheduler() {
        return Schedulers.from(executor);
    }

    protected Scheduler getObserveScheduler() {
        return mainThread.getScheduler();
    }
}
