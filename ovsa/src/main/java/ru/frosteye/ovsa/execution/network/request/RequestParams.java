package ru.frosteye.ovsa.execution.network.request;

import java.io.Serializable;
import java.util.HashMap;



public class RequestParams extends HashMap<String, String> implements Serializable {

    public RequestParams() {
        super();
    }

    public RequestParams(String query) {
        String[] pairs = query.split("&");
        for(String pair: pairs) {
            String[] parts = pair.split("=");
            addParam(parts[0], parts[1]);
        }
    }

    public RequestParams addParam(String key, Object value) {
        put(key, String.valueOf(value));
        return this;
    }

    public String toQueryString() {
        StringBuilder stringBuilder = new StringBuilder();
        for(Entry<String, String> entry: entrySet()) {
            stringBuilder.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return stringBuilder.length() == 0 ? "" : stringBuilder.toString().substring(1);
    }
}
