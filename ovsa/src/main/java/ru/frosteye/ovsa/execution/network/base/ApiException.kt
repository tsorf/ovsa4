package ru.frosteye.ovsa.execution.network.base



class ApiException : RuntimeException {

    var code = 0

    constructor(message: String?, code: Int) : super(message) {
        this.code = code
    }

    constructor(cause: Throwable) : super(cause) {}
}
