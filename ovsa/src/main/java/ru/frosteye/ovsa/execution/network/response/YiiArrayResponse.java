package ru.frosteye.ovsa.execution.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;



public class YiiArrayResponse<T> {

    @SerializedName("_meta")
    private Meta meta;

    public Meta getMeta() {
        return meta;
    }

    @SerializedName("items")
    private List<T> items;

    public List<T> getItems() {
        return items;
    }

    @SerializedName("_links")
    private Links links;

    public Links getLinks() {
        return links;
    }

    public static class Meta {
        @SerializedName("totalCount")
        private int totalCount;
        @SerializedName("pageCount")
        private int pageCount;
        @SerializedName("currentPage")
        private int currentPage;
        @SerializedName("perPage")
        private int perPage;

        public int getTotalCount() {
            return totalCount;
        }

        public int getPageCount() {
            return pageCount;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public int getPerPage() {
            return perPage;
        }
    }

    public static class Link {
        @SerializedName("href")
        private String href;

        public String getUrl() {
            return href;
        }
    }

    public static class Links {
        private Link self;

        public Link getSelf() {
            return self;
        }
    }
}
