package ru.frosteye.ovsa.execution.network.request



data class PaginatedRequest(
        var page: Int = 1,
        var initialLimit: Int = 15,
        var limit: Int = 15
) {

    fun reset() {
        page = 1
        limit = initialLimit
    }

    fun increase() {
        page++
    }
}
