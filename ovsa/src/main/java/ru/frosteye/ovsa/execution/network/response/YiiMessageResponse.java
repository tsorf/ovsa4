package ru.frosteye.ovsa.execution.network.response;

import java.util.List;



public class YiiMessageResponse extends MessageResponse {

    private List<Error> errors;

    public YiiMessageResponse(String message, int code) {
        super(message, code);
    }

    public List<Error> getErrors() {
        return errors;
    }

    @Override
    public String getMessage() {
        if(errors != null && !errors.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            for(Error error: errors) {
                builder.append(error.getField()).append(": ").append(error.getMessage()).append("\r\n");
            }
            return builder.toString();
        }
        return super.getMessage();
    }

    public static class Error {
        private String field;
        private String message;

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }
}
