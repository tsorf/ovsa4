package ru.frosteye.ovsa.execution.network.client



interface IdentityProvider {
    fun provideIdentity(): String?
}
