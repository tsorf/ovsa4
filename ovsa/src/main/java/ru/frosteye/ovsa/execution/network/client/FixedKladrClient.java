package ru.frosteye.ovsa.execution.network.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import cyclone.tools.kladrapi.client.KladrApiClient;
import cyclone.tools.kladrapi.client.KladrApiUrlBuilder;



public class FixedKladrClient extends KladrApiClient {

    private String token, key;

    public FixedKladrClient() {
    }

    public FixedKladrClient(String token, String key) {
        super(token, key);
        this.token = token;
        this.key = key;
    }

    public String queryKladrApi(String params) {
        String response = null;

        try {
            if(params.startsWith("http://")) {
                params = params.substring(params.indexOf("?"));
                params = params.replace("?", "&");
            }

            if(params.contains("&")) {
                params = params.substring(params.indexOf("&"));
            }

            String e = KladrApiUrlBuilder.getBaseUrl(this.token, this.key);
            e = e + params;
            e = e.replace("http", "https");
            URL url = new URL(e);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder responseBuilder = new StringBuilder();

            String inputLine;
            while((inputLine = in.readLine()) != null) {
                responseBuilder.append(inputLine);
            }

            response = responseBuilder.toString();
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        return response;
    }
}
