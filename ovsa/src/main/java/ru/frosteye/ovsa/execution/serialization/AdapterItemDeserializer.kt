package ru.frosteye.ovsa.execution.serialization

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException

import java.lang.reflect.Type

import ru.frosteye.ovsa.presentation.adapter.AdapterItem



@Deprecated("we do not deserialize this way anymore")
abstract class AdapterItemDeserializer<T, Wrapper : AdapterItem<T, *>> : JsonDeserializer<Wrapper> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): Wrapper {

        try {
            val raw = context.deserialize<T>(json, provideType())
            val constructor = provideWrapperType().getConstructor(provideType())
            return constructor.newInstance(raw)
        } catch (e: Exception) {
            e.printStackTrace()
            throw RuntimeException(e)
        }

    }

    protected abstract fun provideType(): Class<T>
    protected abstract fun provideWrapperType(): Class<Wrapper>
}
