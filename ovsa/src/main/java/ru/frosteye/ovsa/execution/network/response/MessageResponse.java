package ru.frosteye.ovsa.execution.network.response;



public class MessageResponse {
    private String message;
    private String error;
    private int code;
    private int status;

    public MessageResponse(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return error != null ? error : message;
    }

    public int getCode() {
        return status != 0 ? status : code;
    }
}
