package ru.frosteye.ovsa.execution.executor




import io.reactivex.Scheduler

/**
 * Representing Android's UI thread
 */
interface MainThread {
    val scheduler: Scheduler
}
