package ru.frosteye.ovsa.execution.network.client

import com.google.gson.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.frosteye.ovsa.BuildConfig
import java.lang.IllegalArgumentException
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit



@Target(AnnotationTarget.CLASS)
annotation class RestApi

interface ApiClient {

    fun <T : Any>apiFor(clazz: Class<T>): T
}

abstract class DistributedRetrofitClient(
    apis: Set<Class<out Any>>
): ApiClient {

    private val map: Map<Class<out Any>, Any> = apis.associateWith { createApi(it) }


    override fun <T : Any> apiFor(clazz: Class<T>): T {
        val result: Any = map[clazz]
            ?: throw IllegalArgumentException("no $clazz in $this")
        if (clazz.isInstance(result)) {
            return result as T
        } else {
            throw IllegalArgumentException("$result is not $clazz")
        }
    }

    protected open fun <T : Any>createApi(forClass: Class<T>): T {
        if (forClass.getAnnotation(RestApi::class.java) == null) {
            throw IllegalArgumentException("$forClass is not RestApi")
        }
        val client = object : BaseRetrofitClient<T>(url(forClass)) {
            override fun apiClass(): Class<T> {
                return forClass
            }

            override fun populateRetrofitBuilder(builder: Retrofit.Builder) {
                this@DistributedRetrofitClient.populateRetrofitBuilder(forClass, builder)
            }

            override fun populateOkHttpBuilder(builder: OkHttpClient.Builder) {
                this@DistributedRetrofitClient.populateOkHttpBuilder(forClass, builder)
            }

            override fun createGsonBuilder(): GsonBuilder {
                return this@DistributedRetrofitClient.createGsonBuilder(
                    forClass,
                    super.createGsonBuilder()
                )
            }

            override fun createClientInterceptors(): MutableList<Interceptor> {
                return this@DistributedRetrofitClient.createClientInterceptors(
                    forClass,
                    super.createClientInterceptors()
                )
            }

            override var identityProvider: IdentityProvider? = null
                get() = identity(forClass)

            override val authHeaderName: String
                get() = authHeaderName(forClass)
            override val authHeaderValuePrefix: String
                get() = authHeaderValuePrefix(forClass)
        }
        return client.api
    }

    protected open fun populateRetrofitBuilder(forClass: Class<out Any>,
                                               builder: Retrofit.Builder) {

    }

    protected open fun populateOkHttpBuilder(forClass: Class<out Any>,
                                             builder: OkHttpClient.Builder) {

    }

    protected open fun createGsonBuilder(forClass: Class<out Any>,
                                         original: GsonBuilder): GsonBuilder {
        return original
    }

    protected open fun createClientInterceptors(forClass: Class<out Any>,
                                                original: MutableList<Interceptor>): MutableList<Interceptor> {
        return original
    }

    protected open fun identity(forClass: Class<out Any>): IdentityProvider? {
        return null
    }

    abstract fun url(forClass: Class<out Any>): String

    protected open fun authHeaderName(forClass: Class<out Any>): String {
        return "Authorization"
    }

    protected open fun authHeaderValuePrefix(forClass: Class<out Any>): String {
        return ""
    }

}

abstract class BaseRetrofitClient<T : Any> {

    lateinit var api: T
    open var identityProvider: IdentityProvider? = null

    protected var token: String? = null
        get() = if (identityProvider != null) {
            identityProvider!!.provideIdentity()
        } else field

    protected val connectTimeout: Int
        get() = 30

    protected val readTimeout: Int
        get() = 30

    protected open val authHeaderName: String
        get() = "Authorization"

    protected open val authHeaderValuePrefix: String
        get() = ""

    open val headers: MutableMap<String, String> = mutableMapOf()


    abstract fun apiClass(): Class<T>

    constructor(baseUrl: String, identityProvider: IdentityProvider? = null) {
        init(baseUrl, identityProvider)
    }

    protected open fun init(baseUrl: String, identityProvider: IdentityProvider? = null) {
        this.identityProvider = identityProvider
        val clientBuilder = OkHttpClient.Builder().apply {

            createClientInterceptors().forEach {
                this.addInterceptor(it)
            }
            connectTimeout(connectTimeout.toLong(), TimeUnit.SECONDS)
            readTimeout(readTimeout.toLong(), TimeUnit.SECONDS)
            populateOkHttpBuilder(this)
        }



        val gson = createGsonBuilder()
                .setExclusionStrategies(TransientExclusionStrategy)
                .create()

        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(clientBuilder.build())

        populateRetrofitBuilder(builder)
        builder.addConverterFactory(GsonConverterFactory.create(gson))


        createCallAdapters().forEach {
            builder.addCallAdapterFactory(it)
        }

        this.api = builder.build().create(apiClass())
    }



    protected open fun populateRetrofitBuilder(builder: Retrofit.Builder) {}

    protected open fun populateOkHttpBuilder(builder: OkHttpClient.Builder) {}

    protected open fun createGsonBuilder(): GsonBuilder {
        return GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
    }

    protected open fun createClientInterceptors(): MutableList<Interceptor> {
        return mutableListOf<Interceptor>().apply {
            add(Interceptor { chain ->
                val builder = chain.request().newBuilder()
                if (token != null) {
                    builder.addHeader(authHeaderName, authHeaderValuePrefix + token!!)
                }
                for ((key, value) in headers) {
                    builder.addHeader(key, value)
                }
                chain.proceed(builder.build())
            })
            if (BuildConfig.DEBUG)
                add(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
    }

    protected open fun createCallAdapters(): List<CallAdapter.Factory> {
        return mutableListOf<CallAdapter.Factory>().apply {
            add(RxJava2CallAdapterFactory.create())
        }
    }
}

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY)
annotation class GsonTransient

object TransientExclusionStrategy : ExclusionStrategy {
    override fun shouldSkipClass(type: Class<*>): Boolean = false
    override fun shouldSkipField(f: FieldAttributes): Boolean =
            f.getAnnotation(GsonTransient::class.java) != null
                    || f.name.endsWith("\$delegate")
}

class BooleanSerializer : JsonSerializer<Boolean>, JsonDeserializer<Boolean> {

    override fun serialize(src: Boolean?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        if (src == null) return JsonNull.INSTANCE
        return JsonPrimitive(if (src) 1 else 0)
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Boolean {
        if (json == null) return false
        val element = json as JsonPrimitive
        return (element.asInt == 1)
    }


}
