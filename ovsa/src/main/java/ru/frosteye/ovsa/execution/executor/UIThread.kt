package ru.frosteye.ovsa.execution.executor


import javax.inject.Inject

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers



class UIThread @Inject
constructor() : MainThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}
