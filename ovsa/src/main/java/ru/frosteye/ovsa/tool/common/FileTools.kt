package ru.frosteye.ovsa.tool.common

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

import okhttp3.ResponseBody



object FileTools {

    fun responseBodyToFile(body: ResponseBody, output: File): Boolean {
        try {

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                val fileSize = body.contentLength()
                var fileSizeDownloaded: Long = 0

                inputStream = body.byteStream()
                outputStream = FileOutputStream(output)

                while (true) {
                    val read = inputStream!!.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)

                    fileSizeDownloaded += read.toLong()

                    Log.d("OvsaDownloader", "file download: $fileSizeDownloaded of $fileSize")
                }

                outputStream.flush()

                return true
            } catch (e: IOException) {
                e.printStackTrace()
                return false
            } finally {
                body.close()
                inputStream?.close()

                outputStream?.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }

    }
}
