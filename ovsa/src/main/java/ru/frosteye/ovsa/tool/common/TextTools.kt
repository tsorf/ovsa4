package ru.frosteye.ovsa.tool.common

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Build
import android.telephony.PhoneNumberUtils
import android.text.Html
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import java.util.*
import android.text.Editable
import android.text.TextWatcher
import java.io.*
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.text.NumberFormat


/**
 * Cuts string source to conform defined length.
 * @param source [String] source.
 * @param length length for resulting string.
 * @return the resulting string.
 */
fun String.cut(length: Int): String {
    return if (this.length <= length) this else String.format("%s...", this.substring(0, length - 3))
}

/**
 * Checks if [TextView]'s content is empty.
 * @param textView [TextView] to check.
 * @return check result
 */
fun TextView.isEmpty(): Boolean {
    return this.text.toString().isEmpty()
}


/**
 * Checks if [TextView]'s content is empty (applying [String.trim]).
 * @param textView [TextView] to check.
 * @return result.
 */
fun TextView.isTrimmedEmpty(): Boolean {
    return this.text.toString().trim { it <= ' ' }.isEmpty()
}

fun TextView.trimmed() : String {
    return text.toString().trim()
}

fun TextView.trimmedOrNull() : String? {
    val value = text.toString().trim()
    return if (value.isEmpty()) null else value
}


object TextTools {



    @JvmStatic
    fun trimParagraph(text: CharSequence): CharSequence {
        var text = text
        if (text.length == 0) return text
        while (text[text.length - 1] == '\n') {
            text = text.subSequence(0, text.length - 1)
        }
        return text
    }

    fun validatePhone(string: String): Boolean {
        return PhoneNumberUtils.isGlobalPhoneNumber(string) && string.length == 12
    }

    fun validateEmail(email: String?): Boolean {
        if (email == null) return false
        val ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
        val p = java.util.regex.Pattern.compile(ePattern)
        val m = p.matcher(email)
        return m.matches()
    }

    fun validateEmail(input: EditText): Boolean {
        return validateEmail(input.trimmed())
    }

    fun overrideFont(context: Context, defaultFontNameToOverride: String, customFontFileNameInAssets: String) {

        val customFontTypeface = Typeface.createFromAsset(context.assets, customFontFileNameInAssets)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val newMap = HashMap<String, Typeface>()
            newMap["serif"] = customFontTypeface
            try {
                val staticField = Typeface::class.java
                        .getDeclaredField("sSystemFontMap")
                staticField.isAccessible = true
                staticField.set(null, newMap)
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }

        } else {
            try {
                val defaultFontTypefaceField = Typeface::class.java.getDeclaredField(defaultFontNameToOverride)
                defaultFontTypefaceField.isAccessible = true
                defaultFontTypefaceField.set(null, customFontTypeface)
            } catch (e: Exception) {
                Log.e(TextTools::class.java.simpleName, "Can not set custom font $customFontFileNameInAssets instead of $defaultFontNameToOverride")
            }

        }
    }

    fun byteArrayToHexString(inarray: ByteArray): String {
        var i: Int
        var j: Int
        var `in`: Int
        val hex = arrayOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")
        var out = ""
        j = 0
        while (j < inarray.size) {
            `in` = inarray[j].toInt() and 0xff
            i = `in` shr 4 and 0x0f
            out += hex[i]
            i = `in` and 0x0f
            out += hex[i]
            ++j
        }
        return out
    }

    fun nameWithoutExtension(fileName: String): String {
        try {
            val parts = fileName.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            return parts[parts.size - 1]
        } catch (e: Exception) {
            return fileName
        }

    }

    fun loadStringFromRawResource(resources: Resources, resId: Int): String {
        val rawResource = resources.openRawResource(resId)
        val content = streamToString(rawResource)
        try {
            rawResource.close()
        } catch (e: IOException) {
        }

        return content
    }

    fun loadStringFromFile(file: File): String {
        val rawResource = file.inputStream()
        val content = streamToString(rawResource)
        try {
            rawResource.close()
        } catch (e: IOException) {
        }

        return content
    }

    private fun streamToString(`in`: InputStream): String {
        var l: String?
        val r = BufferedReader(InputStreamReader(`in`))
        val s = StringBuilder()
        try {
            do {
                l = r.readLine()
                if (l == null) break
                s.append(l + "\n")
            } while (l != null)
        } catch (e: IOException) {
        }

        return s.toString()
    }

    fun stripHtml(html: String): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString()
        } else {
            Html.fromHtml(html).toString()
        }
    }
}

class MoneyTextWatcher(editText: EditText) : TextWatcher {
    private val editTextWeakReference: WeakReference<EditText>

    init {
        editTextWeakReference = WeakReference<EditText>(editText)
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get() ?: return
        val s = editable.toString()
        if (s.isEmpty()) return
        editText.removeTextChangedListener(this)
        val cleanString = s.replace("[$,.]".toRegex(), "")
        val parsed = BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
        val formatted = NumberFormat.getCurrencyInstance().format(parsed)
        editText.setText(formatted)
        editText.setSelection(formatted.length)
        editText.addTextChangedListener(this)
    }
}

fun Context.copyTextToClipboard(text: String, label: String = "Text") {
    (getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager)
        .setPrimaryClip(ClipData.newPlainText(label, text))
}

fun String.md5(): String {
    return try {
        val md = java.security.MessageDigest.getInstance("MD5")
        val array = md.digest(this.toByteArray())
        val sb = StringBuffer()
        for (i in array.indices) {
            sb.append(Integer.toHexString(array[i].toInt() and 0xFF or 0x100).substring(1, 3))
        }
        sb.toString()
    } catch (e: java.security.NoSuchAlgorithmException) {
        e.printStackTrace()
        this
    }
}
