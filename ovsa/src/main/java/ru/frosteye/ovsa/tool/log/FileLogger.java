package ru.frosteye.ovsa.tool.log;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;


public class FileLogger extends SimpleLogger {
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd, HH:mm", Locale.getDefault());
    private File logFile;
    private boolean logcatOnly = false;

    @Inject
    public FileLogger(Context context) {
        super(context);
        String fileName = context.getPackageName() + ".log";
        logFile = new File(Environment.getExternalStorageDirectory(), fileName);
        if(!logFile.exists()) {
            try {
                logcatOnly = !logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                logcatOnly = true;
            }
        }
    }

    public boolean isLogcatOnly() {
        return logcatOnly;
    }

    @Override
    public void debug(String message) {
        super.debug(message);
        write(message, false);
    }

    @Override
    public void error(String message) {
        super.error(message);
        write(message, true);
    }

    @Override
    public void error(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        error(pw.toString());
    }

    private void write(String message, boolean error) {
        try {
            FileOutputStream stream = new FileOutputStream(logFile);
            String logMessage = simpleDateFormat.format(new Date()) +  (error ? " Error: " : " Debug: ") + message + "\n";
            stream.write(logMessage.getBytes());
            stream.close();
        } catch (IOException e) {
            super.error(e.getLocalizedMessage());
        }
    }
}
