package ru.frosteye.ovsa.tool.common

import android.content.Context
import android.content.Intent
import android.net.Uri
import java.lang.Exception


object MarketTools {

    fun goToMarket(context: Context, packageName: String? = null) {
        val pack = packageName ?: context.packageName
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$pack")
                ).apply {
                    addFlags(
                        Intent.FLAG_ACTIVITY_NO_HISTORY or
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK
                    )
                }
            )
        } catch (e: Exception) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$pack")
                )
            )
        }

    }
}