package ru.frosteye.ovsa.tool.common

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask

import java.io.IOException
import java.net.HttpURLConnection
import java.net.InetSocketAddress
import java.net.Socket
import java.net.URL


object NetworkTools {

    const val TYPE_WIFI = 1
    const val TYPE_MOBILE = 2
    const val TYPE_NOT_CONNECTED = 0
    const val NETWORK_STATUS_NOT_CONNECTED = 0
    const val NETWORK_STAUS_WIFI = 1
    const val NETWORK_STATUS_MOBILE = 2

    private var task: AsyncTask<Void, Void, Boolean>? = null


    interface ConnectListener {
        fun onResult(connected: Boolean)
    }

    @SuppressLint("MissingPermission")
    fun getConnectivityStatus(context: Context): Int {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI

            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }

    fun getConnectivityStatusString(context: Context): Int {
        val conn = getConnectivityStatus(context)
        var status = 0
        if (conn == TYPE_WIFI) {
            status = NETWORK_STAUS_WIFI
        } else if (conn == TYPE_MOBILE) {
            status = NETWORK_STATUS_MOBILE
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = NETWORK_STATUS_NOT_CONNECTED
        }
        return status
    }

    fun quickCheckConnection(): Boolean {
        try {
            val timeoutMs = 1500
            val sock = Socket()
            val sockaddr = InetSocketAddress("8.8.8.8", 53)

            sock.connect(sockaddr, timeoutMs)
            sock.close()

            return true
        } catch (e: IOException) {
            return false
        }

    }

    @JvmOverloads
    fun checkActiveInternetConnection(context: Context, listener: ConnectListener, host: String? = null) {
        if (getConnectivityStatus(context) != TYPE_NOT_CONNECTED) {
            if (task != null) {
                task!!.cancel(true)
                task = null
            }
            task = @SuppressLint("StaticFieldLeak")
            object : AsyncTask<Void, Void, Boolean>() {
                override fun doInBackground(vararg params: Void): Boolean? {
                    try {
                        val urlc = URL(
                                host ?: "http://frosteye.ru"
                        ).openConnection() as HttpURLConnection
                        urlc.setRequestProperty("User-Agent", "Test")
                        urlc.setRequestProperty("Connection", "close")
                        urlc.connectTimeout = 1500
                        urlc.connect()
                        return urlc.responseCode == 200
                    } catch (e: IOException) {
                        return false
                    }

                }

                override fun onPostExecute(aBoolean: Boolean?) {
                    listener.onResult(aBoolean!!)
                }
            }
            task!!.execute()

        } else {
            listener.onResult(false)
        }
    }
}
