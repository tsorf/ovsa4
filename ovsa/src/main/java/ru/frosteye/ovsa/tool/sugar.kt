package ru.frosteye.ovsa.tool

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.data.storage.ResourceHelper
import ru.frosteye.ovsa.presentation.view.dialog.Confirm


fun <T1 : Any, T2 : Any, R : Any> multiLet(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}

fun <T1 : Any, T2 : Any, T3 : Any, R : Any> multiLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    block: (T1, T2, T3) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null) block(p1, p2, p3) else null
}

fun <T1 : Any, T2 : Any, T3 : Any, T4 : Any, R : Any> multiLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    p4: T4?,
    block: (T1, T2, T3, T4) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null && p4 != null) block(p1, p2, p3, p4) else null
}

fun <T1 : Any, T2 : Any, T3 : Any, T4 : Any, T5 : Any, R : Any> multiLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    p4: T4?,
    p5: T5?,
    block: (T1, T2, T3, T4, T5) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null && p4 != null && p5 != null) block(
        p1,
        p2,
        p3,
        p4,
        p5
    ) else null
}

fun Int.s(): String {
    return try {
        ResourceHelper.s(this)
    } catch (e: Exception) {
        toString()
    }
}

inline fun <reified T> Any?.cast(): T? {
    if (this == null) return null
    return if (this is T) {
        this
    } else null
}

fun dpToPx(dp: Float): Float {
    return ResourceHelper.dpToPx(dp)
}

fun Context.copyToClipboard(text: CharSequence) {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText(packageName, text)
    clipboard.setPrimaryClip(clip)
}

fun Boolean.toInt(): Int {
    return if (this) 1 else 0
}

/*fun <T>Int.s(vararg args: T): String {
    return try {
        ResourceHelper.s(this, args)
    } catch (e: Exception) {
        toString()
    }
}*/

fun SwipeRefreshLayout.hide(hide: Boolean) {
    if (hide) isRefreshing = false
}

fun View.activate(activated: Boolean) {
    isActivated = activated

}

fun View.select(selected: Boolean) {
    isSelected = selected
}

fun View.enable(enabled: Boolean) {
    isEnabled = enabled
    if (this is ViewGroup) {
        this.children.forEach {
            it.enable(enabled)
        }
    }
}

fun <T : View> Int.inflateAs(context: Context, container: ViewGroup): T {
    return LayoutInflater.from(context).inflate(this, container, false) as T
}

fun <T : View> Int.inflateAs(context: Context): T {
    return LayoutInflater.from(context).inflate(this, null) as T
}

fun Intent.getExtraAction(): Int? {
    val action = this.getIntExtra("action", 0)
    return if (action == 0) null else action
}

fun Intent.setExtraAction(action: Int): Intent {
    this.putExtra("action", action)
    return this
}

fun Intent.putExtra(extra: Parcelable): Intent {
    this.putExtra(extra.parcelName(), extra)
    return this
}

fun <T : Fragment>T.withModels(vararg models: Parcelable): T {
    arguments = Bundle().apply {
        models.forEach {
            putParcelable(it::class.java.name, it)
        }
    }
    return this
}

inline fun <reified T : Parcelable> Intent.getParcelable(): T? {
    return getParcelableExtra(T::class.java.name)
}

inline fun <reified T : Parcelable> Intent.getRequredParcelable(): T {
    val result = getParcelable<T>()
    checkNotNull(result) {
        "Intent doesn't contain required parcelable"
    }
    return result
}


fun Context.startActivityWithModelId(clazz: Class<out Activity>, modelId: String) {
    startActivity(Intent(this, clazz).apply {
        putExtra("modelId", modelId)
    })
}


fun Context.startActivityWithId(clazz: Class<out Activity>, id: Int) {
    startActivity(Intent(this, clazz).apply {
        putExtra("id", id)
    })
}

fun Context.startActivity(clazz: Class<out Activity>) {
    val intent = Intent(this, clazz)
    startActivity(intent)
}

fun Context.startActivity(clazz: Class<out Activity>, vararg payloads: Parcelable?) {
    val intent = Intent(this, clazz)
    payloads.forEachIndexed { i, p ->
        p?.let {
            val name = p::class.java.name
            intent.putExtra(name, p)
        }
    }
    startActivity(intent)
}

fun Context.startActivity(clazz: Class<out Activity>, items: Map<String, Parcelable>) {
    val intent = Intent(this, clazz)
    items.forEach {
        intent.putExtra(it.key, it.value)

    }
    startActivity(intent)
}

fun Context.dialPhone(phone: String) {
    startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)))
}

fun Intent.getModelId(): String? {
    return getStringExtra("modelId")
}

fun Intent.getId(): Int? {
    val result = getIntExtra("id", 0)
    if (result == 0) return null
    return result
}

fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
    val tmp = this[index1]
    this[index1] = this[index2]
    this[index2] = tmp
}

fun Context.confirm(
    title: CharSequence,
    text: CharSequence?,
    @StringRes yesTitle: Int?,
    @StringRes noTitle: Int? = null,
    @StringRes neutralTitle: Int? = null,
    listener: (Int) -> Unit
) {
    val builder = Confirm.create(this)
        .title(title)
        .message(text ?: R.string.ovsa_string_are_you_sure.s())
        .no(noTitle ?: R.string.ovsa_string_cancel) { d, i ->
            d.cancel()
            listener(-1)
        }
        .yes(yesTitle ?: R.string.ovsa_string_ok) { d, i ->
            d.cancel()
            listener(1)
        }
    neutralTitle?.let {
        builder.neutral(it) { d, i ->
            d.cancel()
            listener(0)
        }
    }
    builder.show()

}

fun Context.confirm(@StringRes text: Int, listener: (Boolean) -> Unit) {
    Confirm.create(this)
        .title(text)
        .message(R.string.ovsa_string_are_you_sure)
        .no(R.string.ovsa_string_cancel) { d, i ->
            d.cancel()
            listener(false)
        }
        .yes(R.string.ovsa_string_ok) { d, i ->
            d.cancel()
            listener(true)
        }.show()

}

fun Context.confirm(text: CharSequence, listener: (Boolean) -> Unit) {
    Confirm.create(this)
        .title(text)
        .message(R.string.ovsa_string_are_you_sure)
        .no(R.string.ovsa_string_cancel) { d, i ->
            d.cancel()
            listener(false)
        }
        .yes(R.string.ovsa_string_ok) { d, i ->
            d.cancel()
            listener(true)
        }.show()

}

fun EditText.showKeyboard() {
    requestFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)
        ?.showSoftInput(
            this, InputMethodManager.SHOW_IMPLICIT
        )
}

fun Parcelable.parcelName(): String {
    return this::class.java.name
}