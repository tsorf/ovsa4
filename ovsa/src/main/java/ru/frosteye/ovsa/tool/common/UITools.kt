package ru.frosteye.ovsa.tool.common

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.hardware.Camera
import android.text.InputFilter
import androidx.annotation.ArrayRes
import androidx.appcompat.app.AlertDialog
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StyleRes
import androidx.appcompat.widget.PopupMenu

import java.io.File
import java.io.FileOutputStream

import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.presentation.view.contract.IPrompt
import ru.frosteye.ovsa.tool.s
import ru.frosteye.ovsa.tool.showKeyboard
import java.lang.reflect.Field


object UITools {

    fun Activity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    @JvmStatic
    fun toastLong(context: Context?, message: CharSequence?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @JvmStatic
    fun toastLong(context: Context?, message: Int?) {
        if (context == null || message == null) return
        toastLong(context, context.getString(message))
    }

    fun toastShort(context: Context?, message: CharSequence?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun toastShort(context: Context?, message: Int?) {
        if (context == null || message == null) return
        toastShort(context, context.getString(message))
    }

    @JvmOverloads
    fun simpleAlert(
        context: Context, title: String, message: CharSequence,
        listener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialogInterface, i -> }
    ): AlertDialog {
        return AlertDialog.Builder(context).setTitle(title).setMessage(message)
            .setPositiveButton(R.string.ovsa_string_ok, listener).show()
    }

    @JvmOverloads
    fun alert(
        context: Context,
        title: String,
        message: CharSequence,
        actionTitle: String,
        listener: () -> Unit = fun() {}
    ): AlertDialog {
        return AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(actionTitle) { dialogInterface, i ->
                dialogInterface.cancel()
                listener()
            }.show()
    }

    @JvmOverloads
    fun alert(
        context: Context,
        title: Int,
        message: Int,
        actionTitle: String,
        listener: () -> Unit = fun() {}
    ): AlertDialog {
        return alert(
            context, title.s(), message.s(), actionTitle, listener
        )
    }

    fun simpleAlert(context: Context, title: Int, message: Int): AlertDialog {
        return simpleAlert(
            context,
            context.getString(title),
            context.getString(message),
            DialogInterface.OnClickListener { dialogInterface, i -> })
    }

    fun simpleAlert(
        context: Context, title: Int, message: Int,
        listener: DialogInterface.OnClickListener
    ): AlertDialog {
        return simpleAlert(context, context.getString(title), context.getString(message), listener)
    }

    fun simpleAlert(context: Context, title: Int, message: CharSequence): AlertDialog {
        return simpleAlert(
            context,
            context.getString(title),
            message,
            DialogInterface.OnClickListener { dialogInterface, i -> })
    }

    @Deprecated("don't be stupid")
    fun getResourceDeclareStyleableIntArray(context: Context, name: String): IntArray? {
        fun fields(packageName: String): Array<Field>? = try {
            Class.forName("$packageName.R\$styleable").fields
        } catch (e: Exception) {
            null
        }

        val array = fields(context.packageName) ?: fields(context.packageName.replace(".dev", ""))
        array?.let {
            try {
                for (f in it) {
                    if (f.name == name) {
                        return f.get(null) as IntArray
                    }
                }
            } catch (t: Throwable) {
            }
        }

        return null
    }

    fun simpleAlert(context: Context, title: String, message: Int): AlertDialog {
        return simpleAlert(
            context,
            title,
            context.getString(message),
            DialogInterface.OnClickListener { dialogInterface, i -> })
    }

    @JvmStatic
    fun applyDialogLayoutDimensions(dialog: Dialog, width: Int, height: Int) {
        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        lp.width = width
        lp.height = height
        window?.attributes = lp
    }

    fun selector(
        context: Context, @ArrayRes array: Int,
        selectListener: (String, Int) -> Unit
    ) {
        selector(context, context.resources.getStringArray(array), selectListener)
    }

    fun selector(
        context: Context, array: Array<String>,
        selectListener: (String, Int) -> Unit
    ) {
        AlertDialog.Builder(context)
            .setItems(array) { dialogInterface, i -> selectListener(array[i], i) }
            .show()
    }

    fun selector(
        context: Context, title: String, items: Collection<String>,
        cancelable: Boolean = true,
        @StyleRes theme: Int = 0,
        selectListener: (String, Int) -> Unit
    ) {
        val builder = if (theme != 0) {
            AlertDialog.Builder(context)
        } else {
            AlertDialog.Builder(context, theme)
        }
        val array = items.toTypedArray()
        builder.setTitle(title)
            .setItems(array) { dialogInterface, i -> selectListener.invoke(array[i], i) }
            .setCancelable(cancelable)
            .show()
    }

    fun selector(
        context: Context, title: String, items: Collection<String>,
        cancelable: Boolean = true,
        @StyleRes theme: Int = 0,
        selectListener: (String, Int) -> Unit,
        cancelListener: () -> Unit
    ) {
        val builder = if (theme != 0) {
            AlertDialog.Builder(context)
        } else {
            AlertDialog.Builder(context, theme)
        }
        val array = items.toTypedArray()
        builder.setTitle(title)
            .setItems(array) { dialogInterface, i -> selectListener.invoke(array[i], i) }
            .setCancelable(cancelable)
            .setOnCancelListener {
                cancelListener()
            }
            .show()
    }

    fun selector(
        context: Context, title: String, array: Array<String>,
        @StyleRes theme: Int = 0,
        selectListener: (String, Int) -> Unit
    ) {
        val builder = if (theme != 0) {
            AlertDialog.Builder(context)
        } else {
            AlertDialog.Builder(context, theme)
        }
        builder.setTitle(title)
            .setItems(array) { dialogInterface, i -> selectListener.invoke(array[i], i) }.show()
    }

    fun selector(
        context: Context, title: String, @ArrayRes array: Int,
        @StyleRes theme: Int = 0,
        selectListener: (String, Int) -> Unit
    ) {
        val builder = if (theme != 0) {
            AlertDialog.Builder(context)
        } else {
            AlertDialog.Builder(context, theme)
        }
        val items = context.resources.getStringArray(array)
        builder.setTitle(title)
            .setItems(items) { dialogInterface, i -> selectListener.invoke(items[i], i) }.show()
    }

    fun showPrompt(
        context: Activity, prompt: IPrompt,
        listener: IPrompt.Listener
    ) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(prompt.title())
        val view = LayoutInflater.from(context).inflate(R.layout.view_prompt, null)
        val input = view.findViewById<View>(R.id.view_prompt_input) as EditText
        input.hint = prompt.hint()
        builder.setView(view)
        if (prompt.inputType() != 0) {
            input.inputType = prompt.inputType()
            if (prompt.inputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD
                || prompt.inputType() == InputType.TYPE_NUMBER_VARIATION_PASSWORD
            ) {
                input.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
        if (prompt.maxLength() != 0) {
            input.filters = arrayOf(InputFilter.LengthFilter(prompt.maxLength()))
        }
        prompt.text()?.let {
            input.setText(it)
            input.setSelection(it.length)
            input.requestFocus()
        }
        builder.setCancelable(prompt.cancelable())
        builder.setNegativeButton(if (prompt.negativeButton() == null) context.getString(R.string.ovsa_string_cancel) else prompt.negativeButton()) { dialog, which ->
            dialog.cancel()
            listener.onCancel()
        }
        builder.setPositiveButton(
            prompt.positiveButton(),
            DialogInterface.OnClickListener { dialog, which ->
                if (input.isTrimmedEmpty()) {
                    listener.onCancel()
                    return@OnClickListener
                }
                listener.onResult(input.trimmed())
                dialog.cancel()
            })
        val dialog = builder.create()
        dialog.setOnShowListener {
            input.showKeyboard()
        }
        dialog.show()
    }

    fun confirm(
        context: Context, title: Int,
        simpleConfirmCallback: SimpleConfirmCallback
    ) {
        confirm(context, context.getString(title), null, simpleConfirmCallback)
    }

    fun confirm(
        context: Context, title: Int, message: Int,
        simpleConfirmCallback: SimpleConfirmCallback
    ) {
        confirm(
            context,
            context.getString(title),
            context.getString(message),
            simpleConfirmCallback
        )
    }

    fun confirm(
        context: Context, title: String, message: String?,
        simpleConfirmCallback: SimpleConfirmCallback
    ) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        if (message != null) builder.setMessage(message)
        builder.setPositiveButton(R.string.ovsa_string_ok) { dialogInterface, i -> simpleConfirmCallback.yes() }
        if (simpleConfirmCallback is ConfirmCallback) {
            builder.setNegativeButton(R.string.ovsa_string_cancel) { dialogInterface, i -> simpleConfirmCallback.no() }
        }
        builder.show()
    }

    fun setCameraDisplayOrientation(activity: Activity, camera: Camera) {
        var cameraId = -1
        val numberOfCameras = Camera.getNumberOfCameras()
        for (i in 0 until numberOfCameras) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(i, info)
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i
                break
            }
        }
        if (cameraId == -1) return
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        val rotation = activity.windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }

        var result: Int
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360
            result = (360 - result) % 360  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360
        }
        camera.setDisplayOrientation(result)
    }

    fun saveBitmapToJpgFile(directory: File, name: String, bitmap: Bitmap): File? {
        try {
            if (!directory.exists()) directory.mkdirs()
            val file = File(directory, "$name.jpg")
            val fileOutputStream = FileOutputStream(file)

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
            return file
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    fun saveBitmapToJpgFile(toFile: File, bitmap: Bitmap): File? {
        try {
            if (!toFile.exists()) {
                toFile.createNewFile()
            }
            val fileOutputStream = FileOutputStream(toFile)

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
            return toFile
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    fun resizeBitmap(context: Context, drawableName: String, width: Int, height: Int): Bitmap {
        val imageBitmap = BitmapFactory.decodeResource(
            context.resources,
            context.resources.getIdentifier(drawableName, "drawable", context.packageName)
        )
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    fun getWindowDimens(activity: Activity): Point {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size
    }

    fun showIconsInPopup(popupMenu: PopupMenu) {
        try {
            val fields = popupMenu::class.java.declaredFields
            for (field in fields) {
                if ("mPopup" == field.name) {
                    field.isAccessible = true
                    val menuPopupHelper = field.get(popupMenu)
                    val classPopupHelper = Class.forName(menuPopupHelper.javaClass.name)
                    val setForceIcons = classPopupHelper.getMethod(
                        "setForceShowIcon", Boolean::class.javaPrimitiveType!!
                    )
                    setForceIcons.invoke(menuPopupHelper, true)
                    break
                }
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }

    }

    interface SimpleConfirmCallback {
        fun yes()
    }

    interface ConfirmCallback : SimpleConfirmCallback {
        fun no()
    }
}

fun Dialog.applyLayoutDimensions(width: Int, height: Int) {
    val lp = WindowManager.LayoutParams()
    window?.let {
        lp.copyFrom(it.attributes)
        lp.width = width
        lp.height = height
        it.attributes = lp
    }
}
