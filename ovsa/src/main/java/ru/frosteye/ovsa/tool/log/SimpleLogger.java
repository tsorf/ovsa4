package ru.frosteye.ovsa.tool.log;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import ru.frosteye.ovsa.R;


public class SimpleLogger implements Logger {
    protected Context context;
    private String tag;

    @Inject
    public SimpleLogger(Context context) {
        this.context = context;
        this.tag = context.getString(R.string.ovsa_simple_logger_tag);
    }

    @Override
    public void debug(String message) {
        Log.d(tag, message);
    }

    @Override
    public void error(String message) {
        Log.e(tag, message);
    }

    @Override
    public void error(Throwable throwable) {
        throwable.printStackTrace();
    }
}
