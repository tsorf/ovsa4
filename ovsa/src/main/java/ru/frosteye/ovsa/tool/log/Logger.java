package ru.frosteye.ovsa.tool.log;


public interface Logger {
    /**
     * Logs the message to implementation's output stream with debug tag.
     * @param message message to log.
     */
    void debug(String message);

    /**
     * Logs the message to implementation's output stream with error tag.
     * @param message message to log.
     */
    void error(String message);

    /**
     * Logs source throwable's stack trace to implementation's output stream with error tag.
     * @param throwable {@link Throwable} to log.
     */
    void error(Throwable throwable);
}
