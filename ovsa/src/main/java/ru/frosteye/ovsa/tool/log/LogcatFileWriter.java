package ru.frosteye.ovsa.tool.log;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;



public class LogcatFileWriter {

    public static final String TAG = "LogcatFileWriter";

    public static boolean init(Context context) {
        if (isExternalStorageWritable()) {
            File logFile = new File(Environment.getExternalStorageDirectory(),
                    context.getPackageName() + ".log");
            if(logFile.exists())
                logFile.delete();

            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f " + logFile);
                return true;
            } catch ( IOException e ) {
                e.printStackTrace();
                return false;
            }

        } else {
            Log.w(TAG, "Storage is not writable");
            return false;
        }
    }


    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals( state ) ) {
            return true;
        }
        return false;
    }
}
