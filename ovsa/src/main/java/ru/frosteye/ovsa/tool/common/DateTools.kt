package ru.frosteye.ovsa.tool.common

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.DatePicker
import android.widget.TimePicker
import ru.frosteye.ovsa.R
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


object DateTools {

    private val dottedDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
    private val shortDottedDateFormat = SimpleDateFormat("dd.MM", Locale.getDefault())
    private val monthYearFormat = SimpleDateFormat("MMMM yyyy", Locale.getDefault())
    private val monthYearFormatIso = SimpleDateFormat("yyyy-MM", Locale.getDefault())
    private val dashedDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private val dayLabelFormat = SimpleDateFormat("EEE", Locale.getDefault())
    private val dayMonthFormat = SimpleDateFormat("dd.MM", Locale.getDefault())
    private val monthFormat = SimpleDateFormat("MMMM", Locale.getDefault())
    private val slashedDateFormat = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())
    private val dottedDateFormatWithTime = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
    private val dottedDateFormatWithTimeAndSeconds = SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault())
    private val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    private val hourStartFormat = SimpleDateFormat("HH:00", Locale.getDefault())
    private val hourFormat = SimpleDateFormat("HH", Locale.getDefault())
    private val timeFormatMinutesSeconds = SimpleDateFormat("mm:ss", Locale.getDefault())
    private val dayOfMonthFormat = SimpleDateFormat("dd.MM", Locale.getDefault())
    private val prettyDateFormat = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
    private val prettyDateFormatWithoutYear = SimpleDateFormat("dd MMMM", Locale.getDefault())
    private val prettyDateFormatWithTime = SimpleDateFormat("dd MMMM yyyy, HH:mm", Locale.getDefault())
    private val isoDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    private val birthCalendar = Calendar.getInstance()

    @Deprecated("use the second impl")
    interface DatePickerCallback {
        fun onDateSelected(date: Date)
    }

    interface RangePickerCallback {
        fun onDateSelected(startDate: Date, endDate: Date)
    }

    interface TimePickerCallback {
        fun onTimeSelected(time: String)
        fun onRaw(hour: Int, minute: Int)
    }

    fun formatDottedDateWithTime(date: Date?): String? {
        return if (date == null) null else dottedDateFormatWithTime.format(date)
    }

    fun formatDottedDateWithTimeWithSeconds(date: Date?): String? {
        return if (date == null) null else dottedDateFormatWithTimeAndSeconds.format(date)
    }

    fun formatTime(date: Date?): String? {
        return if (date == null) null else timeFormat.format(date)
    }

    fun formatHourStart(date: Date?): String? {
        return if (date == null) null else hourStartFormat.format(date)
    }

    fun formatTimeCounter(date: Date?): String? {
        return if (date == null) null else timeFormatMinutesSeconds.format(date)
    }

    @JvmStatic
    fun formatDashedDate(date: Date?): String? {
        return if (date == null) null else dashedDateFormat.format(date)
    }

    @JvmStatic
    fun formatDayMonth(date: Date?): String? {
        return if (date == null) null else dayMonthFormat.format(date)
    }

    @JvmStatic
    fun formatMonthYear(date: Date?): String? {
        return if (date == null) null else monthYearFormat.format(date)
    }

    @JvmStatic
    fun formatMonthYearIso(date: Date?): String? {
        return if (date == null) null else monthYearFormatIso.format(date)
    }

    @JvmStatic
    fun formatIsoDate(date: Date?): String? {
        return if (date == null) null else isoDateFormat.format(date)
    }

    @JvmStatic
    fun formatHour(date: Date?): String? {
        return if (date == null) null else hourFormat.format(date)
    }

    fun formatPrettyDate(date: Date?): String? {
        return if (date == null) null else prettyDateFormat.format(date)
    }

    fun formatPrettyShortDate(date: Date?): String? {
        return if (date == null) null else prettyDateFormatWithoutYear.format(date)
    }

    fun formatPrettyDateWithTime(date: Date?): String? {
        return if (date == null) null else prettyDateFormatWithTime.format(date)
    }

    @JvmStatic
    fun formatDottedDate(date: Date?): String? {
        return if (date == null) null else dottedDateFormat.format(date)
    }

    @JvmStatic
    fun formatShortDottedDate(date: Date?): String? {
        return if (date == null) null else shortDottedDateFormat.format(date)
    }

    fun formatDayOfMonth(date: Date?): String? {
        return if (date == null) null else dayOfMonthFormat.format(date)
    }

    fun formatMonth(date: Date?): String? {
        return if (date == null) null else monthFormat.format(date)
    }

    fun formatDayLabel(date: Date?): String? {
        return if (date == null) null else dayLabelFormat.format(date)
    }

    fun parseSlashedDate(date: String): Date? {
        try {
            return slashedDateFormat.parse(date)
        } catch (e: Exception) {
            return null
        }

    }



    fun formatTime(totalSecs: Int): String {
        val hours = totalSecs / 3600
        val minutes = (totalSecs % 3600) / 60
        val seconds = totalSecs % 60

        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    //FIXME говно
    fun calculateAge(date: Date): Int {
        var age: Int = 0
        synchronized(birthCalendar) {
            birthCalendar.time = date
            val today = Calendar.getInstance()
            age = today.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR)
            if (today.get(Calendar.DAY_OF_YEAR) < birthCalendar.get(Calendar.DAY_OF_YEAR)) {
                age--
            }
        }
        return age
    }

    fun getMonths(): List<String> {
        val calendar = Calendar.getInstance().apply {
            set(Calendar.DAY_OF_MONTH, 1)
        }
        return (0..11).map {
            calendar.set(Calendar.MONTH, it)
            monthFormat.format(calendar.time)
        }
    }

    /*



    public static void showDateDialog(Context context, final DatePickerCallback callback) {
        showDateDialog(context, callback, null, null);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Date minDate) {
        showDateDialog(context, callback, null, minDate);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Calendar initial) {
        showDateDialog(context, callback, initial, null);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Calendar initial, Date minDate) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        if(minDate != null) {
            datePicker.setMinDate(minDate.getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
            }
        });
        d.show();
    }


    public static void showDateDialog(Context context, Date minDate, Date maxDate, final DatePickerCallback callback) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(minDate != null) {
            datePicker.setMinDate(minDate.getTime());
        }
        if(maxDate != null) {
            datePicker.setMaxDate(maxDate.getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
            }
        });
        d.show();
    }*/

    /*public static void showDateDialogWithButtons(Context context, final DatePickerCallback callback, Calendar initial) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }*/

    @JvmStatic
    fun showDateDialogWithButtons(context: Context, initial: Calendar?,
                                  fromToday: Boolean?, cancelListener: (() -> Unit)? = null,
                                  listener: ((Date) -> Unit)) {
        val d = Dialog(context)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.ovsa_date_picker, null)
        view.findViewById<View>(R.id.datePickerButtons).setVisibility(View.VISIBLE)
        val cancel = view.findViewById(R.id.datePickerCancel) as Button
        val ok = view.findViewById(R.id.datePickerOk) as Button
        val datePicker = view.findViewById(R.id.datePicker) as DatePicker

        if (initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            )
        }
        if (fromToday != null) {
            if (fromToday) {
                datePicker.minDate = Date().time - 2000
            } else {
                datePicker.maxDate = Date().time
            }
        }
        datePicker.setCalendarViewShown(false)
        d.setContentView(view)
        ok.setOnClickListener {
            val day = datePicker.getDayOfMonth()
            val month = datePicker.getMonth()
            val year = datePicker.getYear()

            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            listener(calendar.time)
            d.cancel()
        }
        cancel.setOnClickListener {
            d.cancel()
            cancelListener?.invoke()
        }
        d.show()
    }

    @JvmStatic
    @Deprecated("use the second impl")
    fun showDateDialogWithButtons(context: Context, callback: DatePickerCallback, initial: Calendar?, fromToday: Boolean,
                                  cancelListener: (() -> Unit)? = null) {
        val d = Dialog(context)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.ovsa_date_picker, null)
        view.findViewById<View>(R.id.datePickerButtons).setVisibility(View.VISIBLE)
        val cancel = view.findViewById(R.id.datePickerCancel) as Button
        val ok = view.findViewById(R.id.datePickerOk) as Button
        val datePicker = view.findViewById(R.id.datePicker) as DatePicker

        if (initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            )
        }
        if (fromToday) {
            datePicker.minDate = Date().time - 2000
        } else {
            datePicker.maxDate = Date().time
        }
        datePicker.setCalendarViewShown(false)
        d.setContentView(view)
        ok.setOnClickListener {
            val day = datePicker.getDayOfMonth()
            val month = datePicker.getMonth()
            val year = datePicker.getYear()

            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            callback.onDateSelected(calendar.time)
            d.cancel()
        }
        cancel.setOnClickListener {
            d.cancel()
            cancelListener?.invoke()
        }
        d.show()
    }

    @JvmStatic
    fun showDateDialogWithButtonsMature(context: Context, callback: DatePickerCallback) {
        val d = Dialog(context)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.ovsa_date_picker, null)
        view.findViewById<View>(R.id.datePickerButtons).setVisibility(View.VISIBLE)
        val cancel = view.findViewById(R.id.datePickerCancel) as Button
        val ok = view.findViewById(R.id.datePickerOk) as Button
        val datePicker = view.findViewById(R.id.datePicker) as DatePicker
        val initial = Calendar.getInstance();
        initial.add(Calendar.YEAR, - 18);
        datePicker.init(
                initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
        )
        datePicker.setCalendarViewShown(false)
        d.setContentView(view)
        ok.setOnClickListener {
            val day = datePicker.getDayOfMonth()
            val month = datePicker.getMonth()
            val year = datePicker.getYear()

            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            callback.onDateSelected(calendar.time)
            d.cancel()
        }
        cancel.setOnClickListener {
            d.cancel()
        }
        d.show()
    }

    /*public static void showDateDialogWithButtonsMature(Context context, final DatePickerCallback callback) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        Calendar initial = Calendar.getInstance();
        initial.add(Calendar.YEAR, - 18);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        datePicker.setMaxDate(initial.getTime().getTime());
        initial.add(Calendar.YEAR, - 80);
        datePicker.setMinDate(initial.getTime().getDate());
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    */

    fun showTimeDialogWithButtons(context: Context, listener: (String, Int, Int) -> Unit) {
        val d = Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        val view = LayoutInflater.from(context).inflate(R.layout.ovsa_time_picker, null)
        view.findViewById<View>(R.id.datePickerButtons).visibility = View.VISIBLE
        val cancel = view.findViewById(R.id.datePickerCancel) as Button
        val ok =  view.findViewById(R.id.datePickerOk) as Button
        val timePicker =  view.findViewById(R.id.timePicker) as TimePicker
        timePicker.setIs24HourView(true);
        d.setContentView(view);
        ok.setOnClickListener {
            val hour: Int
            val min: Int
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1){
                hour = timePicker.hour
                min = timePicker.minute
            } else {
                hour = timePicker.getCurrentHour();
                min = timePicker.getCurrentMinute();
            }
            listener(formatTime(hour, min), hour, min)
            d.cancel();
        };
        cancel.setOnClickListener {
            d.cancel()
        }
        d.show()
    }

    /*

    public static void showTimeDialog(Context context, final TimePickerCallback callback) {
        Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.time_picker, null);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int hour, min;
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = timePicker.getHour();
                    min = timePicker.getMinute();
                } else {
                    hour = timePicker.getCurrentHour();
                    min = timePicker.getCurrentMinute();
                }
                callback.onRaw(hour, min);
                callback.onTimeSelected(formatTime(hour, min));
            }
        });
        d.show();
    }

    */

    fun formatTime(hour: Int, minute: Int): String {
        val h = if (hour < 10) "0$hour" else hour.toString()
        val m = if (minute < 10) "0$minute" else minute.toString()
        return String.format("%s:%s", h, m)
    }

    fun countDaysBetween(d1: Date, d2: Date): Int {
        var daysdiff = 0
        val diff = d2.time - d1.time
        val diffDays = diff / (24 * 60 * 60 * 1000) + 1
        daysdiff = diffDays.toInt()
        return daysdiff
    }

    fun formatMillisecondsToTime(time: Int): String {
        val minutes = time / (60 * 1000)
        val seconds = time / 1000 % 60
        return String.format("%d:%02d", minutes, seconds)
    }

    fun formatSecondsToTime(time: Int): String {
        val minutes = time / 60
        val seconds = time % 60
        return String.format("%d:%02d", minutes, seconds)
    }
}

fun secondsToMinutesSeconds(time: Int): String {
    val minutes = time / 60
    val seconds = time % 60
    return String.format("%d:%02d", minutes, seconds)
}

fun secondsToHoursMinutes(time: Int): String {
    val hours = time / 60 / 60
    val minutes = time / 60 % 60
    return String.format("%d:%02d", hours, minutes)
}

fun minutesToHoursMinutes(time: Int): String {
    val hours = time / 60
    val minutes = time % 60
    return String.format("%d:%02d", hours, minutes)
}
