package ru.frosteye.ovsa.tool.common

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Environment
import androidx.core.graphics.scale
import androidx.exifinterface.media.ExifInterface

import java.io.File
import java.io.FileOutputStream


object ImageTools {
    fun drawableToFile(context: Context, drawable: Drawable): File? {
        try {
            val path = Environment.getExternalStorageDirectory().path +
                    "/Android/data/" + context.packageName + "/.images/"
            val parent = File(path)
            if (!parent.exists()) parent.mkdirs()
            val bitmap = drawableToBitmap(drawable)
            val file = File(
                parent,
                context.getString(context.applicationInfo.labelRes) + System.currentTimeMillis() + ".jpg"
            )
            val outStream = FileOutputStream(file)
            bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
            outStream.flush()
            outStream.close()
            return file
        } catch (e: Exception) {
            return null
        }

    }

    fun bitmapToFile(context: Context, bitmap: Bitmap): File? {
        try {
            val path = Environment.getExternalStorageDirectory().path +
                    "/Android/data/" + context.packageName + "/.images/"
            val parent = File(path)
            if (!parent.exists()) parent.mkdirs()
            val file = File(
                parent,
                context.getString(context.applicationInfo.labelRes) + System.currentTimeMillis() + ".jpg"
            )
            val outStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
            outStream.flush()
            outStream.close()
            return file
        } catch (e: Exception) {
            return null
        }

    }

    fun drawableToBitmap(drawable: Drawable): Bitmap? {
        var bitmap: Bitmap? = null

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            bitmap = Bitmap.createBitmap(
                1,
                1,
                Bitmap.Config.RGB_565
            ) // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.RGB_565
            )
        }

        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }
}

fun File.resizeImageFile(width: Int): File {
    val bitmap = BitmapFactory.decodeFile(path)
    val ratio = bitmap.width / width.toFloat()
    val resized = bitmap.scale(width, (bitmap.height / ratio).toInt())

    val name = "${nameWithoutExtension}_$width.${extension}"
    val copy = File(this.parentFile, name)

    val outStream = FileOutputStream(copy)
    resized.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
    outStream.flush()
    outStream.close()
    return this
}

fun File.resizeTmpByMatrix(size: Int, useAsMaxDimension: Boolean = false): File {

    val exif = ExifInterface(this.path)
    val file = createTempFile(suffix = ".${extension}")
    val bitmap = BitmapFactory.decodeFile(path)

    val oldWidth = bitmap.width
    val oldHeight = bitmap.height
    val newWidth: Int
    val newHeight: Int
    val resized: Bitmap = if (useAsMaxDimension) {
        if (bitmap.width > bitmap.height) {
            val ratio = bitmap.width / bitmap.height.toFloat()
            newWidth = size
            newHeight = (size / ratio).toInt()
            Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.RGB_565)
        } else {
            val ratio = bitmap.height / bitmap.width.toFloat()
            newWidth = (size / ratio).toInt()
            newHeight = size
            Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.RGB_565)
        }
    } else {
        val ratio = bitmap.width / bitmap.height.toFloat()
        newWidth = size
        newHeight = (size / ratio).toInt()
        Bitmap.createBitmap(size, (size / ratio).toInt(), Bitmap.Config.RGB_565)
    }


    val ratioX = newWidth / oldWidth.toFloat()
    val ratioY = newHeight / oldHeight.toFloat()
    val middleX = newWidth / 2.0f
    val middleY = newHeight / 2.0f
    val scaleMatrix = Matrix()
    scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
    val canvas = Canvas(resized)
    canvas.setMatrix(scaleMatrix)
    canvas.drawBitmap(
        bitmap,
        middleX - bitmap.width / 2,
        middleY - bitmap.height / 2,
        Paint(Paint.FILTER_BITMAP_FLAG)
    )
    UITools.saveBitmapToJpgFile(file, resized)
    ExifInterface(file.path).apply {
        setAttribute(ExifInterface.TAG_ORIENTATION, exif.getAttribute(ExifInterface.TAG_ORIENTATION))
        saveAttributes()
    }
    return file
}

@Deprecated("use resizeTmpByMatrix")
fun File.resizeTmp(size: Int, useAsMaxDimension: Boolean = false): File {
    val file = createTempFile(suffix = ".${extension}")
    val bitmap = BitmapFactory.decodeFile(path)
    val resized: Bitmap = if (useAsMaxDimension) {
        if (bitmap.width > bitmap.height) {
            val ratio = bitmap.width / bitmap.height.toFloat()
            Bitmap.createScaledBitmap(bitmap, size, (size / ratio).toInt(), false)
        } else {
            val ratio = bitmap.height / bitmap.width.toFloat()
            Bitmap.createScaledBitmap(bitmap, (size / ratio).toInt(), size, false)
        }
    } else {
        val ratio = bitmap.width / bitmap.height.toFloat()
        Bitmap.createScaledBitmap(bitmap, size, (size / ratio).toInt(), false)
    }
    UITools.saveBitmapToJpgFile(file, resized)
    return file
}
